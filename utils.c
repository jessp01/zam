#include <SDL/SDL.h>

/*****************************************************************************
 *                                                                           *
 *                     Print keys                                            *
 *                                                                           *
 *****************************************************************************/

/* Print modifier info */
void PrintModifiers( SDLMod mod ){
  printf( "Modifers: " );

  /* If there are none then say so and return */
  if( mod == KMOD_NONE ){
	printf( "None\n" );
	return;
  }

  /* Check for the presence of each SDLMod value */
  /* This looks messy, but there really isn't    */
  /* a clearer way.                              */
  if( mod & KMOD_NUM ) printf( "NUMLOCK " );
  if( mod & KMOD_CAPS ) printf( "CAPSLOCK " );
  if( mod & KMOD_LCTRL ) printf( "LCTRL " );
  if( mod & KMOD_RCTRL ) printf( "RCTRL " );
  if( mod & KMOD_RSHIFT ) printf( "RSHIFT " );
  if( mod & KMOD_LSHIFT ) printf( "LSHIFT " );
  if( mod & KMOD_RALT ) printf( "RALT " );
  if( mod & KMOD_LALT ) printf( "LALT " );
  if( mod & KMOD_CTRL ) printf( "CTRL " );
  if( mod & KMOD_SHIFT ) printf( "SHIFT " );
  if( mod & KMOD_ALT ) printf( "ALT " );
  printf( "\n" );
}


/* Print all information about a key event */
void PrintKeyInfo( SDL_KeyboardEvent *key ){
  /* Is it a release or a press? */
  if( key->type == SDL_KEYUP )
	printf( "Release:- " );
  else
	printf( "Press:- " );

  /* Print the hardware scancode first */
  printf( "Scancode: 0x%02X", key->keysym.scancode );
  /* Print the name of the key */
  printf( ", Name: %s", SDL_GetKeyName( key->keysym.sym ) );
  /* We want to print the unicode info, but we need to make */
  /* sure its a press event first (remember, release events */
  /* don't have unicode info                                */
  if( key->type == SDL_KEYDOWN ){
	/* If the Unicode value is less than 0x80 then the    */
	/* unicode value can be used to get a printable       */
	/* representation of the key, using (char)unicode.    */
	printf(", Unicode: " );
	if( key->keysym.unicode < 0x80 && key->keysym.unicode > 0 ){
	  printf( "%c (0x%04X)", (char)key->keysym.unicode,
			  key->keysym.unicode );
	}
	else{
	  printf( "? (0x%04X)", key->keysym.unicode );
	}
  }
  printf( "\n" );
  /* Print modifier info */
  PrintModifiers( key->keysym.mod );
}


/*****************************************************************************
 *                                                                           *
 *                        Get/Putpixel                                       *
 *                                                                           *
 *****************************************************************************/

/*
 * Set the pixel at (x, y) to the given value
 * NOTE: The surface must be locked before calling this!
 */
void putpixel(SDL_Surface *surface, int x, int y, Uint32 pixel)
{
  int bpp = surface->format->BytesPerPixel;
  /* Here p is the address to the pixel we want to set */
  Uint8 *p = (Uint8 *)surface->pixels + y * surface->pitch + x * bpp;

  switch(bpp) {
  case 1:
	*p = pixel;
	break;

  case 2:
	*(Uint16 *)p = pixel;
	break;

  case 3:
	if(SDL_BYTEORDER == SDL_BIG_ENDIAN) {
	  p[0] = (pixel >> 16) & 0xff;
	  p[1] = (pixel >> 8) & 0xff;
	  p[2] = pixel & 0xff;
	} else {
	  p[0] = pixel & 0xff;
	  p[1] = (pixel >> 8) & 0xff;
	  p[2] = (pixel >> 16) & 0xff;
	}
	break;

  case 4:
	*(Uint32 *)p = pixel;
	break;
  }
}


/*
 * Return the pixel value at (x, y)
 * NOTE: The surface must be locked before calling this!
 */
Uint32 getpixel(SDL_Surface *surface, int x, int y)
{
  int bpp = surface->format->BytesPerPixel;
  /* Here p is the address to the pixel we want to retrieve */
  Uint8 *p = (Uint8 *)surface->pixels + y * surface->pitch + x * bpp;

  switch(bpp) {
  case 1:
	return *p;

  case 2:
	return *(Uint16 *)p;

  case 3:
	if(SDL_BYTEORDER == SDL_BIG_ENDIAN)
	  return p[0] << 16 | p[1] << 8 | p[2];
	else
	  return p[0] | p[1] << 8 | p[2] << 16;

  case 4:
	return *(Uint32 *)p;

  default:
	return 0;       /* shouldn't happen, but avoids warnings */
  }
}


/*************************************
 *     Print VideoInfo               *
 *************************************/
void printvideoinfo() {
  const SDL_VideoInfo *vinfo;

  if (vinfo = SDL_GetVideoInfo()) {
	printf("--- Video Info ---\n");
	printf("Is it possible to create hardware surfaces? %s\n",
		   vinfo->hw_available?"yes":"no");
	printf("Is there a window manager available? %s\n",
		   vinfo->wm_available?"yes":"no");
	printf("Are hardware to hardware blits accelerated? %s\n",
		   vinfo->blit_hw?"yes":"no");
	printf("Are hardware to hardware colorkey blits accelerated? %s\n",
		   vinfo->blit_hw_CC?"yes":"no");
	printf("Are hardware to hardware alpha blits accelerated? %s\n",
		   vinfo->blit_hw_A?"yes":"no");
	printf("Are software to hardware blits accelerated? %s\n",
		   vinfo->blit_sw?"yes":"no");
	printf("Are software to hardware colorkey blits accelerated? %s\n",
		   vinfo->blit_sw_CC?"yes":"no");
	printf("Are software to hardware alpha blits accelerated? %s\n",
		   vinfo->blit_sw_A?"yes":"no");
	printf("Are color fills accelerated? %s\n",
		   vinfo->blit_fill?"yes":"no");
	printf("Total amount of video memory in Kilobytes: %d\n",
		   vinfo->video_mem);
	//	printf("Pixel format of the video device: \n",
	//		   vinfo->vfmt);
	printf("------------------\n");

  }


}
