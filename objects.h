#ifndef __OBJECTS_H_
#define __OBJECTS_H_

#include "sprite.h"
#include "animations.h"

/* Moving objects families */
typedef enum {
  FAM_ENEMY,					/* Enemy */
  FAM_BONUS,					/* Bonus */
  FAM_BULLET,					/* Bullet */
  FAM_FLOAT						/* Floating string */
} object_family_t;

/* Moving objects types (per family) */
typedef enum {
  ENEMY_DRONE,
  ENEMY_RUSHER,
  ENEMY_ASTEROID32,
  ENEMY_ASTEROID64,
  // ... to be completed
  ENEMY_NUMBER
} enemy_type_t;

typedef enum {
  BULLET_HERO_MISSILE1,
  BULLET_HERO_MISSILE2,
  BULLET_HERO_MISSILE3,
  BULLET_HERO_HMISSILE,
  // ... to be completed
  BULLETS_NUMBER
} bullet_type_t;

typedef enum {
  BONUS_DIVE,
  BONUS_INVULN,
  BONUS_MISSILE,
  BONUS_SCORE,
  BONUS_SHIELD,
  BONUS_SPEED,
  BONUS_WEAPON,
  BONUS_NUMBER
} bonus_type_t;

/* An object */
typedef struct object {
  object_family_t	family;		/* Object family */
  int			type;			/* To be casted to a *_type_t */
  int			energy;
  unsigned int	alive:1;
  unsigned int	is_dying:1;
  int			anim_frame;
  int			anim_delay;
  sprite_t 		*sprite;
  anim_seq_t	*anim_bmap;		/* animation sequence */
  anim_seq_t	*expl_bmap;		/* explosion sequence */
  union {
	struct object	*target;		/* target (for hmissile) */
  } aux;
  int			dont_update;	/* update slowdown (hmissile) */
} object_t;


#if SDL_BYTEORDER == SDL_BIG_ENDIAN
static unsigned int rmask = 0xff000000,
  gmask = 0x00ff0000,
  bmask = 0x0000ff00,
  amask = 0x000000ff;
#else
static unsigned int rmask = 0x000000ff,
  gmask = 0x0000ff00,
  bmask = 0x00ff0000,
  amask = 0xff000000;
#endif

#endif /* __OBJECTS_H_ */
