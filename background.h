
#include <SDL/SDL.h>

#define MAX_LAYERS 4


typedef struct {
  int	w,h;					/* map size */
  int	bw, bh;					/* map block size */
  unsigned char *data;			/* map data */
  int   brows, bcols;			/* blocks bitmap size */
  SDL_Surface	*blocks;		/* map blocks */
} map_t;

typedef struct {
  int	vertical_speed; 		/* vertical scroll speed */
  int	horizontal_speed; 		/* horizontal scroll speed */
  int	incr_update;			/* map incremental update */
  int	is_bitmapped;			/* whether is a bitmap */
  map_t *map;					/* layer's map */
  SDL_Surface *layer_bitmap;	/* bitmap main buffer */
#ifdef LAYER_DBL_BUFFER
  SDL_Surface *spare_bitmap;	/* bitmap spare buffer */
#endif
  int	curr_x, curr_y;			/* Current x,y map position */
  int	scroll_cnt;				/* Scroll counter */
} layer_data_t;

typedef struct {
  int			num_layers;					/* number of layers */
  layer_data_t  *layer_data[MAX_LAYERS];	/* array of layers' data */
  SDL_Surface	*screen;					/* reference screen */
  int			background;					/* background present */
} background_properties_t;

static background_properties_t props;


/***********************
 *      Macros         *
 ***********************/

#define BG_LAYER	props.layer_data[0]
#define BG_SURFACE	(BG_LAYER->layer_bitmap)
#define BG_CURR_X	(BG_LAYER->curr_x)
#define BG_CURR_Y	(BG_LAYER->curr_y)

#define LAYER(l)    props.layer_data[l]


#ifdef CUSTOM_BLIT
#define DO_BLIT(a,b,c,d)									\
	cust_BlitSurface_opaque(a, (b)->x, (b)->y,				\
							(b)->w, (b)->h, (c), (d)->x, (d)->y);
#define DO_BLIT_FULL(a,b,c,d)									\
	cust_BlitSurface_opaque_cont(a, (b)->x, (b)->y,				\
							     (b)->w, (b)->h, (c), (d)->x, (d)->y);
#else
#define DO_BLIT(a,b,c,d)											\
	if (SDL_BlitSurface(a,b,c,d) < 0)								\
	  fprintf(stderr, "BlitSurface error: %s\n", SDL_GetError());
#define DO_BLIT_FULL(a,b,c,d)										\
	if (SDL_BlitSurface(a,b,c,d) < 0)								\
	  fprintf(stderr, "BlitSurface error: %s\n", SDL_GetError());
#endif

int map_position() {
  return LAYER(1)->curr_y*LAYER(1)->map->bh + LAYER(1)->scroll_cnt;
}

int map_length() {
  return LAYER(1)->map->h*LAYER(1)->map->bh;
}
