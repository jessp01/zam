/***************************************************************************
 * ZAM                                                                     *
 * (C) 2022 Jessex                                                         *
 *                                                                         *
 * Original porting from Xenon2000: Phavon                                 *
 * (C) 2002 Gianluca Insolvibile (branzo@users.sourceforge.net)            *
 * http://phavon.sourceforge.net/                                          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 ***************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <SDL/SDL.h>
#include <time.h>
#include <unistd.h>

#include "zam.h"
#include "sprite.h"
#include "music.h"
#include "font.h"
#include "datafiles.h"
#include "objects.h"
#include "constants.h"
#include "ezini.h"
//#include "background.c"

extern sprite_t bullets[];
void init_gadgets(SDL_Surface *scr);
void draw_gadgets(player_t *player);

/*************
 *  Globals  *
 *************/
SDL_Surface *screen;			/* Main screen */

/* Game global variables */
unsigned int global_hiscore = 0;
int background_pos = 0;			/* Current background position */
player_t *player = NULL;
const char* user;
const char* home_dir;
opts_t opts;

/*****************************************************************************
 *                                                                           *
 *                        Time                                               *
 *                                                                           *
 *****************************************************************************/

#define TICK_INTERVAL    45

static Uint32 next_time;

Uint32 time_left(void)
{
  Uint32 now;

  now = SDL_GetTicks();
  if (next_time <= now)
	return 0;
  else
	return next_time - now;
}

// Returns the local date/time formatted as 19-03-2021 11:11:11
char* get_formatted_time() {

    time_t rawtime;
    struct tm* timeinfo;

    time(&rawtime);
    timeinfo = localtime(&rawtime);

    // Must be static, otherwise won't work
    static char _retval[20];
    strftime(_retval, sizeof(_retval), "%d-%m-%Y %H:%M:%S", timeinfo);

    return _retval;
}

void write_score(char* score)
{
    ini_entry_list_t list = NULL;
    AddEntryToList(&list, user, get_formatted_time(), score);

    char scores_path[256];
    snprintf(scores_path, sizeof(scores_path), "%s/%s", home_dir, ".zam_scores.ini");
    printf("\nWriting %s\n", scores_path);
    printf("=======================\n");

    if (!access(scores_path, F_OK) == 0) {
	if (0 != MakeINIFile(scores_path, list))
	{
	    printf("Error making %s file\n", scores_path);
	}
    }else{
	AddEntryToFile(scores_path,list);
    }

    FreeList(list);
}

int pausescreen(void)
{
    SDL_Event event;
    SDLKey key;
    int done, quit;

    /* Stop music: */

#ifndef NOSOUND
    if (opts.audio){
	Mix_PauseMusic();
    }
#endif

    /* Display “PAUSED” Message: */
    draw_string(FONT_16, SCREEN_WIDTH/2-64, SCREEN_HEIGHT/2-8, " PAUSED ");
    /* Wait for keypress: */

    done = 0;
    quit = 0;

    do{
	while (SDL_PollEvent(&event)){
	    if (event.type == SDL_QUIT){
		/* Quit event! */
		quit = 2;
	    }else if (event.type == SDL_KEYDOWN){
		/* A keypress! */
		key = event.key.keysym.sym;
		if (key == SDLK_p){
		    done = 1;
		}else if (key == SDLK_ESCAPE || key == SDLK_q){
		/* ESCAPE: Quit! */
		    quit = 1;
		}
	    }
	}
    }while (quit == 0 && done == 0);

    /* Erase message: */
    draw_string(FONT_16, 180, 100, "      ");

    /* Unpause music: */

#ifndef NOSOUND
    if (opts.audio){
	Mix_PauseMusic();
	Mix_ResumeMusic();
    }
#endif
    return(quit);
}

void reset_player();
void move_player();
int draw_player();

void usage() 
{
  printf("Options:\n"
		 "-fullscreen\t\tfullscreen mode\n"
		 "-background [spd]\t\tbackground (scrolling at spd pixels/frame)\n"
		 "-layers <n>\t\tuse n layers\n"
		 "-audio <n>\t\taudio mode (0=no audio, 1=sfx, 2=music, 3=both)\n"
		 );
  exit(0);
}


int parse_options(int argc, char **argv) 
{


  while (argc) {
	if ( !strncmp(argv[0], "-fullscreen", 11) ||
		 !strncmp(argv[0], "-full", 5) ){
	  opts.fullscreen=1;
	}else if (!strncmp(argv[0], "-autoshoot", 9)) {
	  opts.autoshoot=1;  
	}else if (!strncmp(argv[0], "-layers", 7)) {
	  if (argc>1) {
		opts.layers = atoi(argv[1]);
		if (opts.layers>4) {
		  printf("Maximum number of layers is 4\n");
		  return -1;
		}
		argc--;
		argv++;
	  } else {
		free(opts.layers);
		free(opts.fullscreen);
		free(opts.background);
		free(opts.background_speed);
		free(opts.incr);
		free(opts.audio);
		free(opts.autoshoot);
		usage();
	  }
	} else if (!strncmp(argv[0], "-help", 5)) {
	    free(opts.layers);
	    free(opts.fullscreen);
	    free(opts.background);
	    free(opts.background_speed);
	    free(opts.incr);
	    free(opts.audio);
	    free(opts.autoshoot);
	  usage();
	} else if (!strncmp(argv[0], "-background", 11)) {
	  opts.background = 1;
	  if ((argc>1) && (argv[1][0] != '-')) {
		opts.background_speed = atoi(argv[1]);
		argc--;
		argv++;
	  } else
		opts.background_speed = 0;
	} else if (!strncmp(argv[0], "-incr", 5)) {
	  opts.incr = 1;
	} else if (!strncmp(argv[0], "-audio", 5)) {
	  if ((argc>1) && (argv[1][0] != '-')) {
		opts.audio = atoi(argv[1]);
		printf("Audio is %i\n",atoi(argv[1]));
		argc--;
		argv++;
	  }
	}
	argc--;
	argv++;
  }

  printf("background speed %d\n", opts.background_speed);

  return 0;
}




/***********************************************
 *                                             *
 *            Player functions                 *
 *                                             *
 ***********************************************/
inline void reset_player() 
{
  HERO(player)->anim_delay = HERO(player)->expl_bmap->speed;
  HERO(player)->anim_frame = 0;
  HERO(player)->energy = MAX_HERO_ENERGY;
  HERO(player)->is_dying = 0;
  HERO_SPR(player)->image = HERO(player)->anim_bmap->surface[3];
  sprite_setpos(HERO_SPR(player), SCREEN_WIDTH/2 - 32, SCREEN_HEIGHT-90);
  player->bullet_type = BULLET_HERO_MISSILE1;	/* Missile type (0,1,2) */
  player->hmissile = 0;
  player->name = user;
}

inline void move_player() 
{
  Uint8 *keys;

  if (!HERO(player)->is_dying) {
	keys = SDL_GetKeyState(NULL);
	
	if (keys[KEY_UP])
	  if (HERO_SPR(player)->y - HERO_SPR(player)->spdy > 0)
		HERO_SPR(player)->y -= HERO_SPR(player)->spdy;
	
	if (keys[KEY_DOWN])
	  if ((HERO_SPR(player)->y + 
		   HERO_SPR(player)->h + HERO_SPR(player)->spdy) < SCREEN_HEIGHT)
		HERO_SPR(player)->y += HERO_SPR(player)->spdy;
	
		if (keys[KEY_LEFT])
		  if (HERO_SPR(player)->x - HERO_SPR(player)->spdx > 0)
			HERO_SPR(player)->x -= HERO_SPR(player)->spdx;
		
		if (keys[KEY_RIGHT])
		  if ((HERO_SPR(player)->x + 
			   HERO_SPR(player)->w + HERO_SPR(player)->spdx) < SCREEN_WIDTH)
			HERO_SPR(player)->x += HERO_SPR(player)->spdx;
  }
}


inline int draw_player() 
{

  static int doze = 10, dozing = 0;

  if (dozing) {
	if (--doze)
	  return 1;
	doze = 10;
	dozing = 0;
	reset_player();
  }

  if (HERO(player)->is_dying) {

	HERO_SPR(player)->image = 
	  HERO(player)->expl_bmap->surface[HERO(player)->anim_frame];

	if (--HERO(player)->anim_delay == 0) {
	  HERO(player)->anim_delay = HERO(player)->expl_bmap->speed;
	  if (++HERO(player)->anim_frame == HERO(player)->expl_bmap->n) {
		if (--player->lives == 0)
		  return 0;
		dozing = 1;
	  }
	}

  }
  sprite_draw(HERO_SPR(player));

  return 1;
}



/***********************************************
 *                                             *
 *       Init & shutdown functions             *
 *                                             *
 ***********************************************/
int init_global() 
{

  /* Default values */
  opts.layers = 1;
  opts.fullscreen = 0;
  opts.background = 1;
  opts.background_speed = 0;
  opts.incr = 0;
  //opts.audio = 0;

  init_animations();
  if (opts.audio)
	init_music(opts.audio);
  init_map("test", screen, opts.layers, opts.incr);
  if (opts.background)
	init_background(FILE_BACKGROUND, screen);
  init_script(map_length());

  init_font(screen);
  init_enemies(screen);
  init_bullets(screen);
  init_gadgets(screen);
  init_bonus(screen);
}

void shutdown_global() 
{
  //  release_background(); XXX reinserire
  release_map();
  release_font();
  destroy_script();
  shutdown_music();
}

void init_game() 
{

  player = (player_t *)calloc(1, sizeof(player_t));
  HERO(player) = (object_t *)calloc(1, sizeof(object_t));
  HERO_SPR(player) = sprite_init_empty(64, 64, screen);
  HERO_SPR(player)->spdx = 10;
  HERO_SPR(player)->spdy = 10;
  HERO(player)->anim_bmap = get_animation(ANIM_HERO);
  HERO(player)->expl_bmap = get_animation(ANIM_EXPLOSION_64);

  reset_player();
  reset_bullets();
  reset_enemies();
  reset_gadgets();
  reset_bonus();
  if (opts.background)
	reset_background(screen);
  reset_map(opts.layers);

  background_pos = 0;

  player->lives = 3;
  player->score = 0;
}

void shutdown_game() 
{
  free(HERO(player));
  free(player);
}


/*****************************************************************************
 *                                                                           *
 *                       Main ciofek                                         *
 *                                                                           *
 *****************************************************************************/

int main(int argc, char **argv) 
{

  home_dir = getenv("HOME");
  user = getenv("USER");
  int x, y, i;
  Uint32 yellow;
  SDL_Event event;
  
  int gameloop = 1, gameover, gameover_cnt;
  int flags;
#ifdef SYNCRO_LOOP
  int remain, tot_remain=0, min_remain=TICK_INTERVAL, nloops=0;
  FILE *fstats;
  fstats = fopen("stats.txt", "w");
#endif

  printf("ZAM v" ZAM_VERSION "\n");

  if (parse_options(argc, argv))
	exit(0);


  /********************
   *   SDL Init       *
   ********************/
  
  if( SDL_Init(SDL_INIT_VIDEO) < 0 ) {
	fprintf(stderr,
			"Couldn't initialize SDL: %s\n", SDL_GetError());
	exit(1);
  }

  atexit(SDL_Quit);

  printf("SDL initialized.\n");
  SDL_WM_SetCaption(GNAME, "gfx/galaxy2.bmp");
  SDL_WM_SetIcon(SDL_LoadBMP("gfx/galaxy2.bmp"), NULL);

#ifdef EIGHT_BPP
  flags = SDL_DOUBLEBUF + SDL_HWSURFACE 
	+ ((opts.fullscreen)?SDL_FULLSCREEN:0);
  screen = SDL_SetVideoMode(SCREEN_WIDTH, SCREEN_HEIGHT, 8, flags);
#else
  flags = SDL_DOUBLEBUF | SDL_HWSURFACE | ((opts.fullscreen)?SDL_FULLSCREEN:0);
  /* + SDL_ANYFORMAT */
  screen = SDL_SetVideoMode(SCREEN_WIDTH, SCREEN_HEIGHT, 32, flags);
  //  SDL_SetColorKey(screen, SDL_SRCCOLORKEY+SDL_RLEACCEL, 
  //				  SDL_MapRGB(screen->format, 0, 0, 0));
#endif
  if (screen == NULL) {
	fprintf(stderr, "Couldn't set 640x480x8 video mode: %s\n",
			SDL_GetError());
	exit(1);
  }
  printf("Set 640x480 at %d bits-per-pixel mode\n",
		 screen->format->BitsPerPixel);

  printvideoinfo();
  SDL_ShowCursor(0);



  /******************************
   * Initialize game structures *
   ******************************/
  init_global();


  /********************
   *     Main loop    *
   ********************/

  while(1) {
	if (title_screen(screen, &opts) == -1){
	  break;
	}
	gameover = 0;
	gameover_cnt = 30;
	gameloop = 1;
	init_game();
	while (gameloop) {
	  
	  if (!gameover) {
		if (opts.autoshoot){
		    new_bullet(player->bullet_type, 
			HERO_SPR(player)->x + HERO_SPR(player)->w/2 - 8, 
			HERO_SPR(player)->y + 8);
		    SDL_Delay(0.100);
		}
		while (SDL_PollEvent(&event)) {
		  switch (event.type) {
			case SDL_KEYDOWN:
			  //		printf( "Key press detected\n" );
			  switch (event.key.keysym.sym) {
				case SDLK_ESCAPE:
				case SDLK_q:
				  gameover = 1;
				  break;
				case SDLK_p:
				  pausescreen();
				  break;
				case SDLK_a:
				  if (opts.autoshoot){
				    opts.autoshoot = 0;
				  }else{
				    opts.autoshoot = 1;
				  }				    
				  break;
				case KEY_FIRE:
				  if (!HERO(player)->is_dying)
					new_bullet(player->bullet_type, 
					    HERO_SPR(player)->x + HERO_SPR(player)->w/2 - 8, 
					    HERO_SPR(player)->y + 8);
				  break;
				case KEY_FIRE2:
				  // TBD: we should check the number of outstanding homing
				  // missiles before firing a new one
				  if (!HERO(player)->is_dying)
					new_bullet(BULLET_HERO_HMISSILE, 
					    HERO_SPR(player)->x + HERO_SPR(player)->w/2 - 8, 
					    HERO_SPR(player)->y + 8);
				  break;
				default:
				  break;
			  }
			  break;
			  
			case SDL_KEYUP:
			  //		printf( "Key release detected\n" );
			  //		PrintKeyInfo( &event.key );
			  break;
			  
			case SDL_QUIT:
			  //			  gameloop = 0;
			  gameover = 1;
			  break;
			  
			default:
			  break;
		  }
		}
		move_player();
	  }else{
		draw_string(FONT_16, SCREEN_WIDTH/2-64, SCREEN_HEIGHT/2-8, "Game Over");
		if (player->score > global_hiscore){
		    global_hiscore = player->score;
		}
		int length = snprintf(NULL, 0,"%d",player->score);

		if (!gameover_cnt--){
		  if (player->score > 0){
		    char score[length + 1];
		    sprintf(score, "%d", player->score);
		    write_score(score);
		  }
		  gameloop = 0;
		}
	 }

#ifdef SYNCRO_LOOP
	  next_time = SDL_GetTicks() + TICK_INTERVAL;
#endif

	  play_music();
	
	  /*  update screen  */
	  if (opts.background){
		scroll_background(0, opts.background_speed);
	  }else {
		SDL_FillRect(screen, NULL, 0); /* XXX memset may be faster here? */
	  }

	  for (i=1; i<=opts.layers; i++){
		scroll_map(i);
	  }
	  /* Screen gadgets */
	  update_bonus();
	  update_floats();
	  draw_back_gadgets(player);

	  /* Draw hero ship */
	  if (!gameover){
		gameover |= (draw_player() ? 0 : 1);
	  }
	  /* Update & draw bullets */
	  update_bullets();
	  draw_bullets();

	  /* Update & draw enemies */
	  update_enemies();
	  draw_enemies();

	  /* Scores */
	  draw_front_gadgets(player);

	
	  SDL_Flip(screen);

	  /* Check for collisions */
	  if (check_collisions()) {
		if (HERO(player)->energy <= 0) {
		  HERO(player)->is_dying = 1;
		  sound_hero_explosion();
		}
	  }
	  check_bonus(player);

	  /* Hiscore */
	  if (player->score > global_hiscore) {
		// play some nice jingle ?
	  }


	  /* Execute script */
	  run_script(map_position());

#ifdef SYNCRO_LOOP
	  tot_remain += remain = time_left();
	  if (remain<min_remain)
		min_remain = remain;
	  nloops++;
	  //	printf("next time: %d, time left: %d\n", next_time, remain);
	  fprintf(fstats, "%d\n", remain);
	  SDL_Delay(remain);
	  next_time += TICK_INTERVAL;
#endif
	
	}	/* game while (gameloop) */

	shutdown_game();
  } /* global loop */

  /* Release all resources */
  shutdown_global();

  SDL_Quit();

#ifdef SYNCRO_LOOP
  printf("Average time left: %f\n", (double)tot_remain/(double)nloops);
  printf("Minimum remain time: %d\n", min_remain);
  fclose(fstats);
#endif
  exit(0);
}
