#include <sys/time.h>

#define PROFILE_START							\
{												\
  struct timeval _t1,_t2,_tdiff;					\
  memset(&_t1, 0, sizeof(struct timeval));		\
  memset(&_t2, 0, sizeof(struct timeval));		\
  gettimeofday(&_t1, NULL);

#define PROFILE_END													\
  gettimeofday(&_t2, NULL);											\
  timersub(&_t2, &_t1, &_tdiff);										\
  printf("PROFILING: time: %f msecs\n", 				\
		 1000*((float)_tdiff.tv_sec+(float)_tdiff.tv_usec/1000000));	\
}

