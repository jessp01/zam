
#include "zam.h"
#include "objects.h"
#include "constants.h"

void new_object(object_family_t family, int t,
				int x, int y, int param1, int param2) {

  switch (family) {
	case FAM_ENEMY:
	  {
		enemy_type_t type = (enemy_type_t)t;
		switch (type) {
		  case ENEMY_RUSHER:
		  case ENEMY_DRONE:
			new_enemy(type,
					  (x==0x10000) ? rand()%640 : x, 
					  (y==0x10000) ? rand()%480 : y, 
					  param1, param2);
			break;
		  case ENEMY_ASTEROID32:
		  case ENEMY_ASTEROID64:
			new_enemy(type,
					  (x==0x10000) ? rand()%640 : x, 
					  (y==0x10000) ? rand()%480 : y, 
					  param1, param2);
			break;
		  default:
			fprintf(stderr, "Unknown enemy type %d\n", type);
		}
	  }
	  break;
	case FAM_BONUS:
	  {
		bonus_type_t type = (bonus_type_t)t;
		new_bonus(x, y, 0, 2, type);
	  }
	  break;
	case FAM_BULLET:
	  break;
	default:
	  fprintf(stderr, "Unknown object family %d\n", family);
  }
}


int check_collisions() {

  /* Extern variables */
  extern player_t *player;
  extern sprite_t bullets[];
  extern object_t  *enemies[];

  int i, dmg, pts, ret=0;

  /* Collision between enemy and hero */
  if (!HERO(player)->is_dying)
	for (i=0; i<MAX_ENEMIES; i++)
	  if (enemies[i]->alive)
		if (sprite_collide(enemies[i]->sprite, HERO_SPR(player))) {
		  HERO(player)->energy--;
		  //		sound_explosion();
		  ret++;
		}
  
  /* Collision between enemy and bullet */
  for (i=0; i<MAX_ENEMIES; i++) {
	if (enemies[i]->alive && !enemies[i]->is_dying)
	  if (dmg = is_hit(enemies[i]->sprite)) {
		enemies[i]->energy -= dmg;
		if (enemies[i]->energy <= 0) {
		  pts = enemy_score(enemies[i]->type);
		  new_float(enemies[i]->sprite->x + enemies[i]->sprite->w/2, 
					enemies[i]->sprite->y + enemies[i]->sprite->h/2,  
					abs(enemies[i]->sprite->spdx) > 2 ? 2 : 
					                         enemies[i]->sprite->spdx,
					enemies[i]->sprite->spdy, "%d", pts);
		  enemies[i]->is_dying = 1;
		  enemies[i]->anim_frame = 0;
		  enemies[i]->anim_delay = enemies[i]->expl_bmap->speed;
		  if (enemies[i]->type == ENEMY_ASTEROID32) {
			enemies[i]->sprite->x += 16;
			enemies[i]->sprite->y += 16;
		  } else if (enemies[i]->type == ENEMY_ASTEROID64) {
			int xdev;
			enemies[i]->sprite->x += 32;
			enemies[i]->sprite->y += 32;
			if (!(xdev = enemies[i]->sprite->spdy/2))
			  xdev = 1;
			new_enemy(ENEMY_ASTEROID32,
					  enemies[i]->sprite->x+8,
					  enemies[i]->sprite->y,
					  xdev, enemies[i]->sprite->spdy);
			new_enemy(ENEMY_ASTEROID32,
					  enemies[i]->sprite->x-8, 
					  enemies[i]->sprite->y, 
					  -xdev, enemies[i]->sprite->spdy);
		  }
		  player->score += pts;
		  sound_explosion();
		  ret++;
		}
	  }
  }

  return ret;

}

