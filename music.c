
#include <SDL/SDL.h>
#include <SDL/SDL_mixer.h>

#include "datafiles.h"
Mix_Music *music;
Mix_Chunk *shot;
Mix_Chunk *explode, *explode_hero;

void play_music();
void sound_hero_explosion();
void sound_shot();
void sound_explosion();
static int audio_mode;

#include "music.h"

inline void play_music() {

  if (!(audio_mode & 2))
	return;

  if (!Mix_PlayingMusic())
	Mix_PlayMusic(music, 0);
}

inline void sound_explosion() {

  if (!(audio_mode & 1))
	return;

  Mix_PlayChannel(1, explode, 0);
}

inline void sound_hero_explosion() {

  if (!(audio_mode & 1))
	return;

  Mix_PlayChannel(1, explode_hero, 0);
}

inline void sound_shot() {

  if (!(audio_mode & 1))
	return;

  Mix_PlayChannel(0, shot, 0);
}

void init_music(int mode) {

  audio_mode=mode;

  if (!mode)
	return;

  if (Mix_OpenAudio(44100, AUDIO_S16SYS, 2, 2048) < 0) {
	fprintf(stderr, "Warning: Couldn't set 44100 Hz 16-bit audio\n"
			" - Reason: %s\n", SDL_GetError());
  }

  if (mode & 2)
	if (!(music = Mix_LoadMUS(FILE_MUSIC_SOUNDTRACK))) {
	  fprintf(stderr, "Warning: can't load music [%s]\n",
			  SDL_GetError());
	}

  if (mode & 1) {
	if (!(shot = Mix_LoadWAV(FILE_SOUND_MISSILE))) {
	  fprintf(stderr, "Warning: can't load shot sound [%s]\n",
			  SDL_GetError());
	}
	
	if (!(explode = Mix_LoadWAV(FILE_SOUND_SMALLEXPL))) {
	  fprintf(stderr, "Warning: can't load explosion sound [%s]\n",
			  SDL_GetError());
	}

	if (!(explode_hero = Mix_LoadWAV(FILE_SOUND_HEROEXPL))) {
	  fprintf(stderr, "Warning: can't load explosion sound [%s]\n",
			  SDL_GetError());
	}
  }

}

void shutdown_music() {

  if (!audio_mode)
	return;

  if (audio_mode & 2) {
	Mix_FadeOutChannel(-1, 500);
	Mix_FadeOutMusic(500);
	SDL_Delay(500);
	Mix_FreeMusic(music);
  }

  if (audio_mode & 1) {
	Mix_FreeChunk(shot);
	Mix_FreeChunk(explode);
  }

}

