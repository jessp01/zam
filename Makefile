GNAME='ZAM'
CFLAGS=`sdl-config --cflags` -g 
LFLAGS=`sdl-config --libs` -lSDL_mixer -lm
OPTFLAGS=-O3 -funroll-loops

# PREFIX is environment variable, but if it is not set, then set default value
ifeq ($(PREFIX),)
    PREFIX := /usr/local
endif

# SYNCRO_LOOP  use SDL_Delay() at each refresh interval
# EIGHT_BPP    use 8 bpp screen
# CUSTOM_BLIT  use custom blit routines instead of SDL ones
# CUSTBLIT_USE_MEMCPY   use memcpy() for custom blit
# CUSTBLIT_CONTIGUOS    use a single memcpy() for the whole area when possible
# CUSTBLIT_ASM  enables some asm code (must be switched off for some CPUs,
#                                      e.g. AMD K6-II)
# LEVEL_MAP_PRECALC  precalculate level maps
# NDEBUG		turn off asserts

CONF = -DSYNCRO_LOOP \
       -DCUSTOM_BLIT \
       -DCUSTBLIT_USE_MEMCPY \
       -DCUSTBLIT_CONTIGUOS  \
#       -DCUSTBLIT_ASM \
#       -DLEVEL_MAP_PRECALC \
#       -DLAYER_DBL_BUFFER \ (slower!!!)
#       -DEIGHT_BPP   \
#       -DRAWCOPY     
#		-DNDEBUG

SOURCES = custblit.c zam.c sprite.c utils.c background.c weapon.c enemy.c\
          animations.c music.c script.c font.c objects.c title.c gadgets.c ezini.c

OBJS = $(SOURCES:.c=.o)


$(GNAME): $(OBJS) sprite.h
	gcc $(OBJS) -o $(GNAME) $(LFLAGS)

custblit.o: custblit.c
	gcc -c $(CONF) $(CFLAGS) $(OPTFLAGS) custblit.c -o custblit.o

background.o: background.c background.h
	gcc -c $(CONF) $(CFLAGS) $(OPTFLAGS) background.c -o background.o

.c.o:
	gcc -c $(CONF) $(CFLAGS) $(OPTFLAGS) $< -o $@

clean:
	@rm -f *.o $(GNAME)

install: ZAM
	install -d $(DESTDIR)/$(PREFIX)/bin
	install -m 755 ZAM $(DESTDIR)/$(PREFIX)/bin/

depend:
	makedepend $(CFLAGS) $(SOURCES)

.PHONY: all
all: $(GNAME)


# DO NOT DELETE
