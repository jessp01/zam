
#include <stdarg.h>
#include <SDL/SDL.h>

#include "zam.h"
#include "sprite.h"
#include "font.h"
#include "datafiles.h"
#include "constants.h"

/******************************************
 *                                        *
 *               Screen gadgets           *
 *                                        *
 ******************************************/

extern int global_hiscore;

static SDL_Surface *screen;

static object_t *floats[MAX_FLOATS];
#define F(i) floats[i]
#define FSPR(i) (floats[i]->sprite)

static object_t *boni[MAX_BONI];
#define BN(i) boni[i]
#define BNSPR(i) (boni[i]->sprite)

/*
 * Energy bar
 */

#define ENERGY_BAR_HEIGHT 8
#define ENERGY_BAR_WIDTH  130

void draw_energy_bar(int energy) {
  SDL_Rect dstrect;
  int col;

  if (energy<=0) 
	return;

  dstrect.x = 5;
  dstrect.y = screen->h - 5 - ENERGY_BAR_HEIGHT;
  dstrect.w = (float)ENERGY_BAR_WIDTH/(float)MAX_HERO_ENERGY*energy;
  dstrect.h = ENERGY_BAR_HEIGHT;
  if (energy > 0.4*MAX_HERO_ENERGY)
	col = SDL_MapRGB(screen->format, 0x3b, 0xe4, 0x46);
  else if (energy > 0.2*MAX_HERO_ENERGY)
	col = SDL_MapRGB(screen->format, 0xff, 0xcb, 0x7d);
  else
	col = SDL_MapRGB(screen->format, 0xff, 0x2e, 0x1b);

  SDL_FillRect(screen, &dstrect, col);

}


/*
 * Ships
 */
static sprite_t *life_icon = NULL;

void draw_lives(int lives) {
  int i;
  
  /*for (i=0; i<ships; i++) {
	sprite_setpos(life_icon, 10+life_icon->w*i, screen->h - 50);
	sprite_draw(life_icon);
  }*/
  draw_string(FONT_16, 5, 450, "Lives:%d", lives);
}


/*
 * Score, hiscore and gadgets
 */
void draw_front_gadgets(player_t *p) {
  int i;
  
  draw_string(FONT_16, SCREEN_WIDTH/2-24, 0, p->name);
  draw_string(FONT_16, SCREEN_WIDTH/2-48, 20, "%07d", p->score);
  printf("My score is: %d\n", p->score);
  draw_string(FONT_16, 5, 0, "High Score");
  draw_string(FONT_16, 5, 20, "%07d", global_hiscore);
  printf("My high score is: %d\n", global_hiscore);

  for (i=0; i<MAX_FLOATS; i++) {
	if (F(i)->alive) {
	  sprite_draw(FSPR(i));
	  if (--F(i)->energy <= 0)
		F(i)->alive = 0;
	}
  }

  for (i=0; i<MAX_BONI; i++)
	if (BN(i)->alive)
	  sprite_draw(BNSPR(i));

}

void draw_back_gadgets(player_t *p) {

  draw_string(FONT_8, -1, 0, "ZAM " ZAM_VERSION);
  draw_string(FONT_8, -1, -1, "%d", map_position());

  draw_energy_bar(HERO(p)->energy);
  draw_lives(p->lives);
}


/*
 * Public functions
 */

void update_floats() {
  int i;

  for (i=0; i<MAX_FLOATS; i++) {
	if (F(i)->alive) {
	  FSPR(i)->x += FSPR(i)->spdx;
	  FSPR(i)->y += FSPR(i)->spdy;
	  if (FSPR(i)->x < 0 || FSPR(i)->x > FSPR(i)->surf->w ||
		  FSPR(i)->y < 0 || FSPR(i)->y > FSPR(i)->surf->h)
		F(i)->alive = 0;
	}
  }
}

void new_float(int x, int y, int spdx, int spdy, const char *format, ...) {
  int i;
  va_list ap;
  va_start(ap, format);

  for (i=0; i<MAX_FLOATS; i++) {
	if (!F(i)->alive) {
	  F(i)->alive = 1;
	  F(i)->energy = 5;
	  vdraw_string_surf(FSPR(i)->image, FONT_8, 0, 0, format, ap);
	  FSPR(i)->spdx = spdx;
	  FSPR(i)->spdy = spdy;
	  sprite_setpos(FSPR(i), x, y);

	  va_end(ap);
	  return;
	}
  }

  va_end(ap);
}

void reset_gadgets() {
  int i;

  for (i=0; i<MAX_FLOATS; i++) {
	F(i)->family = FAM_FLOAT;
	F(i)->alive = 0;
	F(i)->is_dying = 0;
	F(i)->energy = 0;
	F(i)->anim_bmap = NULL;
	F(i)->expl_bmap = NULL;
	F(i)->anim_frame = 0;
  }

}

void init_gadgets(SDL_Surface *scr) {
  int i;

  screen = scr;
  life_icon = sprite_init(FILE_LIFE_HERO, screen);

  for (i=0; i<MAX_FLOATS; i++) {
	F(i) = (object_t *)malloc(sizeof(object_t));
	FSPR(i) = sprite_init_empty(100, 8, screen);
	SDL_FillRect(FSPR(i)->image, NULL, 
				 SDL_MapRGB(FSPR(i)->image->format, 0xff, 0x00, 0xff));
  }
  reset_gadgets();

}



/************************************************
 *                                              *
 *                  Hero bonuses                *
 *                                              *
 ************************************************/

void new_bonus(int x, int y, int spdx, int spdy, int type) {
  int i;

  for (i=0; i<MAX_BONI; i++) {
	if (!BN(i)->alive) {
	  BN(i)->alive = 1;
	  BN(i)->energy = 0;
	  BN(i)->type = type;
	  BNSPR(i)->spdx = spdx;
	  BNSPR(i)->spdy = spdy;
	  switch (type) {
		case BONUS_DIVE:
		  BN(i)->anim_bmap = get_animation(ANIM_BONUS_DIVE);
		  break;
		case BONUS_INVULN:
		  BN(i)->anim_bmap = get_animation(ANIM_BONUS_INVULN);
		  break;
		case BONUS_MISSILE:
		  BN(i)->anim_bmap = get_animation(ANIM_BONUS_MISSILE);
		  break;
		case BONUS_SCORE:
		  BN(i)->anim_bmap = get_animation(ANIM_BONUS_SCORE);
		  break;
		case BONUS_SHIELD:
		  BN(i)->anim_bmap = get_animation(ANIM_BONUS_SHIELD);
		  break;
		case BONUS_SPEED:
		  BN(i)->anim_bmap = get_animation(ANIM_BONUS_SPEED);
		  break;
		case BONUS_WEAPON:
		  BN(i)->anim_bmap = get_animation(ANIM_BONUS_WEAPON);
		  break;
		default:
		  printf("unknown bonus type %d\n", type);
	  }
	  BN(i)->anim_delay = BN(i)->anim_bmap->speed;
	  BN(i)->anim_frame = 0;
	  sprite_setpos(BNSPR(i), x, y);
	  return;
	}
  }
}

void update_bonus() {
  int i;

  for (i=0; i<MAX_BONI; i++) {
	if (BN(i)->alive) {
	  BNSPR(i)->x += BNSPR(i)->spdx;
	  BNSPR(i)->y += BNSPR(i)->spdy;

	  if (((BNSPR(i)->x<0 && BNSPR(i)->spdx<=0) || 
		   (BNSPR(i)->x > BNSPR(i)->surf->w - BNSPR(i)->w)) ||
		  ((BNSPR(i)->y<0 && BNSPR(i)->spdy<=0) || 
		   (BNSPR(i)->y > BNSPR(i)->surf->h - BNSPR(i)->h)))
		BN(i)->alive=0;

	  /* Roll animation */
	  if (BN(i)->anim_bmap) {
		BNSPR(i)->image = BN(i)->anim_bmap->surface[BN(i)->anim_frame];
		if (--BN(i)->anim_delay == 0) {
		  BN(i)->anim_delay = BN(i)->anim_bmap->speed;
		  if (++BN(i)->anim_frame == BN(i)->anim_bmap->n)
			BN(i)->anim_frame = 0;
		}
	  }
	}
  }
}

void check_bonus(player_t *p) {
  int i;

  for (i=0; i<MAX_BONI; i++) {
	if (BN(i)->alive) {
	  if (sprite_collide(BNSPR(i), HERO_SPR(p))) {
		BN(i)->alive = 0;
		printf(" got bonus, type %d\n",
			   BN(i)->type);
		switch (BN(i)->type) {
		  case BONUS_DIVE:
			// p->dives++;
			break;
		  case BONUS_INVULN:
			// enable invuln
			break;
		  case BONUS_MISSILE:
			if (++p->hmissile > MAX_HERO_HMISSILE)
			  p->hmissile = MAX_HERO_HMISSILE;
			break;
		  case BONUS_SCORE:
			p->score += 10000;
			break;
		  case BONUS_SHIELD:
			HERO(p)->energy += MAX_HERO_ENERGY>>2;
			if (HERO(p)->energy > MAX_HERO_ENERGY)
			  HERO(p)->energy = MAX_HERO_ENERGY;
			break;
		  case BONUS_SPEED:
			// increase speed
			break;
		  case BONUS_WEAPON:
			if (++p->bullet_type > BULLET_HERO_MISSILE3)
			  p->bullet_type = BULLET_HERO_MISSILE3;
			break;
		}
		// play sound ?
	  }
	}
  }
}


void reset_bonus() {
  int i;

  for (i=0; i<MAX_BONI; i++) {
	BN(i)->family = FAM_BONUS;
	BN(i)->type = 0;
	BN(i)->alive = 0;
	BN(i)->is_dying = 0;
	BN(i)->energy = 0;
	BN(i)->anim_bmap = NULL;
	BN(i)->expl_bmap = NULL;
	BN(i)->anim_frame = 0;
  }

}

void init_bonus(SDL_Surface *scr) {
  int i;

  screen = scr;

  for (i=0; i<MAX_BONI; i++) {
	BN(i) = (object_t *)malloc(sizeof(object_t));
	BNSPR(i) = sprite_init_empty(32, 32, screen);
	SDL_FillRect(BNSPR(i)->image, NULL, 
				 SDL_MapRGB(BNSPR(i)->image->format, 0xff, 0x00, 0xff));
  }
  reset_bonus();

}

