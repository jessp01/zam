#include <string.h>
#include <sys/time.h>
#include <SDL/SDL.h>

#include "zam.h"
#include "objects.h"
#include "background.h"

//static background_properties_t props;

/*****************************************************************************
 *                                                                           *
 *                      Background                                           *
 *                                                                           *
 *****************************************************************************/

void copy_block(int layer, SDL_Surface *dst, int blk, unsigned int val, int dest_row);
void reset_background(SDL_Surface *screen) {
  SDL_Rect srcrect;

  BG_CURR_X = 0;
  BG_CURR_Y = BG_SURFACE->h - SCREEN_HEIGHT;
  BG_LAYER->scroll_cnt = 0;
  BG_LAYER->horizontal_speed = 0;
  BG_LAYER->vertical_speed = 0;

  srcrect.x = 0;
  srcrect.y = BG_CURR_Y;
  srcrect.w = SCREEN_WIDTH;
  srcrect.h = SCREEN_HEIGHT;
    
  /* Blit onto the screen surface */
  if (SDL_BlitSurface(BG_SURFACE, &srcrect, screen, NULL) < 0)
	fprintf(stderr, "BlitSurface error: %s\n", SDL_GetError());
  
  SDL_UpdateRect(screen, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
}

int init_background(char *file_name, SDL_Surface *screen)
{
  SDL_Surface *dummy;
  //  SDL_Rect srcrect;

  BG_LAYER = (layer_data_t *)malloc(sizeof(layer_data_t));
  if (!props.layer_data)
	return -1;


  props.background = 1;
  props.screen = screen;
  dummy = SDL_LoadBMP(file_name);
  BG_SURFACE = SDL_DisplayFormat(dummy);
  SDL_FreeSurface(dummy);

  SDL_SetAlpha(BG_SURFACE, 0, 0);

  if (!BG_SURFACE) {
	fprintf(stderr, "Couldn't load %s: %s\n", file_name, SDL_GetError());
	return -1;
  }
  
  if (BG_SURFACE->format->palette && screen->format->palette) {
    SDL_SetColors(screen, BG_SURFACE->format->palette->colors, 0,
                  BG_SURFACE->format->palette->ncolors);
  }

  BG_LAYER->map = NULL;
  BG_LAYER->is_bitmapped = 1; /* not used */

}

void release_background() {

  SDL_FreeSurface(BG_SURFACE);

  /* XXX free other malloc'ed structures */

}

void scroll_background(int spdx, int spdy) {

  SDL_Surface *screen = props.screen;
  SDL_Rect srcrect, dstrect;

  /* Currently looped scroll works only in the Y direction */
  BG_CURR_Y -= spdy;
  if (BG_CURR_Y < 0)
	BG_CURR_Y += BG_SURFACE->h;

  if (BG_CURR_Y >
	  BG_SURFACE->h - SCREEN_HEIGHT) {

	srcrect.x = BG_CURR_X;
 	srcrect.y = BG_CURR_Y;
 	srcrect.h = BG_SURFACE->h - BG_CURR_Y;
 	srcrect.w = SCREEN_WIDTH;
    dstrect.x = 0;
	dstrect.y = 0;
	DO_BLIT_FULL(BG_SURFACE, &srcrect, screen, &dstrect);

	srcrect.y = 0;
	srcrect.x = BG_CURR_X;
	srcrect.h = SCREEN_HEIGHT - (BG_SURFACE->h - BG_CURR_Y);
	srcrect.w = SCREEN_WIDTH;
	dstrect.y = BG_SURFACE->h - BG_CURR_Y;
	dstrect.x = 0;
	DO_BLIT_FULL(BG_SURFACE, &srcrect, screen, &dstrect);
  } else {
	srcrect.x = BG_CURR_X;
	srcrect.y = BG_CURR_Y;
	srcrect.w = SCREEN_WIDTH;
	srcrect.h = SCREEN_HEIGHT;
	dstrect.x = 0;
	dstrect.y = 0;

	{
	  static int nsamples, bmin=10000, bmax, bavg;
	  int btime;
	  struct timeval t1,t2,tdiff;
	  memset(&t1, 0, sizeof(struct timeval));
	  memset(&t2, 0, sizeof(struct timeval));
	  gettimeofday(&t1, NULL);
	  DO_BLIT_FULL(BG_SURFACE, &srcrect, screen, &dstrect);
	  gettimeofday(&t2, NULL);
	  timersub(&t2, &t1, &tdiff);
	  //	  printf(__FUNCTION__" blit time: %f msecs\n", 
	  //			 1000*((float)tdiff.tv_sec+(float)tdiff.tv_usec/1000000));
	  btime = (int)(100000*((float)tdiff.tv_sec+(float)tdiff.tv_usec/1000000));
	  if (btime<bmin)
		bmin = btime;
	  else if (btime>bmax)
		bmax = btime;
	  bavg = (bavg*nsamples + btime)/++nsamples;
	  //	  printf(__FUNCTION__" blit time [10*nsec units]: min %d, max %d, avg %d\n",
	  //			 bmin, bmax, bavg);
	}
  }

  /* spdx is supposed to be 0 */
  BG_CURR_X += spdx;
  
}

/*****************************************************************************
 *                                                                           *
 *                      MAP handling functions                               *
 *                                                                           *
 *****************************************************************************/
SDL_Surface *map_precalc(int layer);

map_t *read_map(char *file_name, int layer) {

  int i,j,dummy;
  map_t *map;
  SDL_Surface *tmpsurf;
  char map_bitmap_name[100];

  FILE *fp = fopen(file_name, "r");

  if (!fp)
	return NULL;

  map = (map_t *)malloc(sizeof(map_t));
  if (!map) {
	fclose(fp);
	return NULL;
  }

  fread(&dummy, 4, 1, fp);
  if (dummy == 0x78563412) {
	/* binary map */
	/* Map format (big endian order):
	 * 00000000: MAGIC (0x12345678)
	 * 00000004: <block width> <block height>
	 * 0000000C: <map width> <map height>
	 * 00000014: <bitmap rows> <bitmap cols>
	 * 0000001C: <bitmap name> [zero-padded to 32 bytes]
	 * 0000003C: <map data>
	 */
	fread(&map->bw, 1, 4, fp);
	fread(&map->bh, 1, 4, fp);
	fread(&map->w, 1, 4, fp);
	fread(&map->h, 1, 4, fp);
	fread(&map->brows, 1, 4, fp);
	fread(&map->bcols, 1, 4, fp);
	fread(map_bitmap_name, 1, 32, fp);
	map->bw = ntohl(map->bw);
	map->bh = ntohl(map->bh);
	map->w = ntohl(map->w);
	map->h = ntohl(map->h);
	map->brows = ntohl(map->brows);
	map->bcols = ntohl(map->bcols);

	map->data = (unsigned char*)malloc(map->w * map->h * sizeof(unsigned char));
	if (!map->data) {
	  fclose(fp);
	  free(map);
	  return NULL;
	}
	if (fread(map->data, map->w*map->h, 1, fp) != map->w*map->h)
	  fprintf(stderr, "Warning: map file too short\n");
	fclose(fp);

  } else {
	
	rewind(fp);
	fscanf(fp, "%d %d\n", &map->bw, &map->bh);
	fscanf(fp, "%d %d\n", &map->w, &map->h);
	fscanf(fp, "%s\n", map_bitmap_name);
	fscanf(fp, "%d %d\n", &map->brows, &map->bcols);
	
	map->data = (unsigned char*)malloc(map->w * map->h * sizeof(unsigned char));
	if (!map->data) {
	  fclose(fp);
	  free(map);
	  return NULL;
	}
	for (i=0; i<map->h; i++) {
	  fread(map->data + i*map->w, 1, map->w, fp);
	  fread(&dummy, 1, 1, fp);	/* skip newline */
	}
	fclose(fp);
  }

  /* Read blocks from file or create them internally
   * if file does not exist.
   */
  tmpsurf = SDL_LoadBMP(map_bitmap_name);
  if (tmpsurf) {
	SDL_SetColorKey(tmpsurf, SDL_SRCCOLORKEY+SDL_RLEACCEL,
					SDL_MapRGB(tmpsurf->format, 0xff, 0x00, 0xff));
	map->blocks = SDL_DisplayFormat(tmpsurf);
	SDL_FreeSurface(tmpsurf);

	{ /* temp: fill in the first block with transparency */
	  SDL_Rect src;
	  src.x = 0;
	  src.y = 0;
	  src.w = map->bw;
	  src.h = map->bh;
	  SDL_FillRect(map->blocks, &src, 
				   SDL_MapRGB(map->blocks->format, 0xff, 0x00, 0xff));
	}

  } else {
	/* Create fake blocks */
	int i,j,bi,bj;
	unsigned char *buffer;

	tmpsurf = SDL_CreateRGBSurface(SDL_SWSURFACE,
								   map->bw*map->bcols, map->bh*map->brows,
								   32, rmask, gmask, bmask, amask);
	SDL_SetColorKey(tmpsurf, SDL_SRCCOLORKEY+SDL_RLEACCEL,
					SDL_MapRGB(tmpsurf->format, 0xff, 0, 0xff));
	map->blocks = SDL_DisplayFormat(tmpsurf);
	SDL_FreeSurface(tmpsurf);

	/* We assume 4 Bytes/pixel here */
	for (bi=0; bi<map->brows; bi++)
	  for (bj=0; bj<map->bcols; bj++) {
		printf("Layer %d: creating block at %d,%d\n", layer, bi, bj);
		buffer = map->blocks->pixels 
		  + (bi*map->bh)*map->blocks->pitch + 4*(bj*map->bw);
		for (i=0; i<map->bh; i++)
		  for (j=0; j<map->bw; j++)
			switch (bj) {
			case 0:
			  *(Uint32 *)(buffer + map->blocks->pitch*i + 4*j) = 
				(j<i)?0x505050*layer:0xff00ff;
			  break;
			case 1:
			  *(Uint32 *)(buffer + map->blocks->pitch*i + 4*j) = 
				(j>i)?0x505050*layer:0xff00ff;
			  break;
			case 2:
			  *(Uint32 *)(buffer + map->blocks->pitch*i + 4*j) = 0xff00ff;
			  break;
			default:
			  *(Uint32 *)(buffer + map->blocks->pitch*i + 4*j) = 
				0xf0f0f0;
			}
	  }
  }

  /* dump map
  printf("block size: %d %d, map size: %d %d\n",
		 map->bw, map->bh, map->w, map->h);
  for (i=0; i<map->h; i++) {
	for (j=0; j<map->w; j++)
	  printf("%c", map->data[i*map->w + j]);
	printf("\n");
  }
  */
  
  return map;
}

void reset_map(int nlayers) {
  int l;
  for (l=1; l<=nlayers; l++) {
	LAYER(l)->curr_x = 0;
	LAYER(l)->curr_y = 0;
	LAYER(l)->scroll_cnt = 0;
#ifndef LEVEL_MAP_PRECALC
	SDL_FillRect(LAYER(l)->layer_bitmap, NULL, 0xff00ff);
#ifdef LAYER_DBL_BUFFER
	SDL_FillRect(LAYER(l)->spare_bitmap, NULL, 0xff00ff);
#endif
#endif
  }
}

int init_map(char *base_name, SDL_Surface *screen, int nlayers, int incr)
{
  char file_name[80];
  int l;
  SDL_Rect srcrect;
  SDL_Surface *temp;

  if (nlayers<1 || nlayers>MAX_LAYERS)
	return -1;

  /* layer 0 is always the background (even if not present) */
  for (l=1; l<=nlayers; l++) {
	LAYER(l) = (layer_data_t *)malloc(sizeof(layer_data_t));
	if (!LAYER(l)) {
	  return -1;
	}

	sprintf(file_name, "%s%d.map", base_name, l);
	if ((LAYER(l)->map = read_map(file_name, l)) == NULL) {
	  free(LAYER(l));
	  return -1;
	}

#ifdef LEVEL_MAP_PRECALC
	/* XXX */
	LAYER(l)->layer_bitmap = map_precalc(l);

#else
	/* layer's main surface */
	temp =
	  SDL_CreateRGBSurface(SDL_HWSURFACE, 
						   LAYER(l)->map->bw * (LAYER(l)->map->w+2),
						   LAYER(l)->map->bh * (LAYER(l)->map->h+2),
						   32, rmask, gmask, bmask, 0);
	SDL_SetColorKey(temp, SDL_SRCCOLORKEY, 
					SDL_MapRGB(temp->format, 0xff, 0, 0xff));
	//	  LAYER(l)->layer_bitmap = SDL_DisplayFormatAlpha(temp);
	//	  SDL_SetColorKey(temp, 0, SDL_MapRGB(temp->format, 0, 0, 0));
	LAYER(l)->layer_bitmap = SDL_DisplayFormat(temp);
	SDL_FreeSurface(temp);
	SDL_FillRect(LAYER(l)->layer_bitmap, NULL, 0xff00ff);

#ifdef LAYER_DBL_BUFFER
	/* layer's spare surface */
	temp =
	  SDL_CreateRGBSurface(SDL_HWSURFACE, 
						   LAYER(l)->map->bw * (LAYER(l)->map->w+2),
						   LAYER(l)->map->bh * (LAYER(l)->map->h+2),
						   32, rmask, gmask, bmask, 0);
	SDL_SetColorKey(temp, SDL_SRCCOLORKEY, 
					SDL_MapRGB(temp->format, 0xff, 0, 0xff));
	//	  LAYER(l)->layer_bitmap = SDL_DisplayFormatAlpha(temp);
	//	  SDL_SetColorKey(temp, 0, SDL_MapRGB(temp->format, 0, 0, 0));
	LAYER(l)->spare_bitmap = SDL_DisplayFormat(temp);
	SDL_FreeSurface(temp);
	SDL_FillRect(LAYER(l)->spare_bitmap, NULL, 0xff00ff);
#endif

#endif /* LAYER_MAP_PRECALC */

	if (!LAYER(l)->layer_bitmap) {
	  free(LAYER(l));
	  //	free(props.layers);
	  // free_map(LAYER(l)->map);
	  return -1;
	}
	LAYER(l)->horizontal_speed = 0;
	LAYER(l)->vertical_speed = l+1; /* TEMP spd = layer level + 1 */
	if (incr)
	  LAYER(l)->incr_update = 
		LAYER(l)->map->w / (LAYER(l)->map->bh/LAYER(l)->vertical_speed);
  }
  reset_map(nlayers);
  
  props.num_layers = nlayers;
  props.screen = screen;

}

void release_map() {

  /* XXX To be completed !!! */
  SDL_FreeSurface(LAYER(1)->layer_bitmap);
  
}

inline
void copy_block(int layer, SDL_Surface *dst, int blk, unsigned int val, 
				int dest_row) {
  int i,j, idx;
  int bpp = dst->format->BytesPerPixel;
  map_t *map = LAYER(layer)->map;
  int bw = map->bw, bh = map->bh;
  unsigned char *base, *src;
  int bskip, sskip;
  SDL_Rect srcrect, dstrect;

  /* TEMP code - shall be substituted by map associations */
  /* XXX valid only for textual (fake) maps... */
  idx = (val=='_') ? 0 : val;

  /* If block idx is 0 (which currently corresponds to
   * a transparent block) we may skip it */
  if (!idx)
	return;

#ifdef OLDONE					/* valid for textual maps */
  switch (val) {
  case '\\':
	idx = 0;
	break;
  case '/':
	idx = 1;
	break;
  case '-':
	idx = 2;
	break;
  default:
	idx = 0;
  }
#endif

#if defined (CUSTOM_BLIT) && defined (CUSTBLIT_USE_MEMCPY)
  src = map->blocks->pixels 
	+ (idx%map->bcols)*map->bw*4 + (idx/map->bcols)*map->bh*map->blocks->pitch;
  base =  dst->pixels + blk*bpp*bw + dest_row*bh*dst->pitch;
  /*
  if (bpp!=4)
	printf("CAZZAROLA (bpp=%d)\n", bpp);
  */

  bskip = dst->pitch;
  sskip = map->blocks->pitch;

  for (i=0; i<map->bh; i++) {
	memcpy(base, src, map->bw*4);
	base += bskip;
	src  += sskip;
  }

#else

  srcrect.w = bw;
  srcrect.h = bh;
  srcrect.x = (idx%map->bcols)*map->bw;
  srcrect.y = (idx/map->bcols)*map->bh;	  
  dstrect.x = blk*bw;
  dstrect.y = dest_row*bh;

  SDL_BlitSurface(map->blocks, &srcrect, dst, &dstrect);
#endif
}

#ifndef LEVEL_MAP_PRECALC

/* Nota: la update_map() causa dei picchi negativi sul time left. Se il picco
 * raggiunge lo 0, lo scroll scatta. Se pero' l'average time left e' positivo, 
 * e' possibile eliminare i picchi eseguendo in anticipo una parte del lavoro 
 * della update_map() (pari a 1/<velocita' layer>).
 * In queste condizioni (e con una una velocita' layer sufficientemente bassa)
 * e' possibile eliminare gli scatti. Forse. Chissa'. Puo' darsi.
 * Nota 2: tanto casino invano. Anche con l'update incrementale ci sono i picchi
 * a 0. Evidentemente la colpa e' del for nella update_map(). Si potrebbe rendere
 * anche quello incrementale...
 * Nota 3: mettendo un 'return' all'inizio di copy_block() cambia (poco) il tempo 
 * medio ma non i picchi a 0. I picchi sono dovuti ai Blit nella update_map() !!
 */
void prepare_row_buffer(int layer, int increment) {

  int i, nblocks;
  SDL_Rect srcrect, dstrect;
  map_t *map = LAYER(layer)->map;
#ifdef LAYER_DBL_BUFFER
  SDL_Surface *surface = LAYER(layer)->spare_bitmap;
#else
  SDL_Surface *surface = LAYER(layer)->layer_bitmap;
#endif

  static int howmany[MAX_LAYERS], complete[MAX_LAYERS];

  /* XXX Nota: potrebbe essere sbagliata. Confrontare con incremental_scroll(). */
  if (howmany[layer]==0) {
	dstrect.x = 0;
	dstrect.y = 0;
	dstrect.w = surface->w - 2*map->bw;
	dstrect.h = map->bh;
	SDL_FillRect(surface, &dstrect, SDL_MapRGB(surface->format, 0xff, 0, 0xff));
  }

  if (increment == -1) {
	//	printf(__FUNCTION__ "[%d] precalcolati %d blocchi (complete=%d)\n", 
	//		   layer, howmany[layer], complete[layer]);
	complete[layer] = 0;
	nblocks = map->w;
  } else {
	nblocks = howmany[layer] + increment;
	nblocks = (nblocks>map->w) ? map->w : nblocks;
	howmany[layer] += increment;
  }

  //  printf(__FUNCTION__ "[%d] increment=%d, nblocks=%d\n", 
  //		 layer, increment, nblocks);

  for (i=0; i<nblocks; i++)
	copy_block(layer, surface, i, 
			   map->data[LAYER(layer)->curr_y*map->w + i], 0);

  if (howmany[layer] >= map->w) {
	howmany[layer] = 0;
	complete[layer] = 1;
	//	printf(__FUNCTION__ "[%d] COMPLETE\n", layer);
  }
}


#ifdef LAYER_DBL_BUFFER
void incremental_scroll(int layer, int increment) {
  int y;
  int loop_start, loop_end;
  SDL_Rect srcrect, dstrect;
  map_t *map = LAYER(layer)->map;
  SDL_Surface *dummy, *surface = LAYER(layer)->spare_bitmap;

  static int howmany[MAX_LAYERS], complete[MAX_LAYERS];

  //  printf(__FUNCTION__ "[%d] incr=%d, howmany=%d\n", layer, increment, howmany[layer]);

  if (increment == -1) {
	//	printf(__FUNCTION__ "[%d] precalcolate %d righe (complete=%d)\n", 
	//		   layer, howmany[layer], complete[layer]);
	if (complete[layer]) {
	  loop_start = loop_end = 0;
	} else {
	  loop_start = map->h-1 - howmany[layer];
	  loop_end = 0;
	}
	complete[layer] = 0;
	howmany[layer] = 0;
  } else {
	loop_start = map->h-1 - howmany[layer];
	loop_end = loop_start - increment +1;
	loop_end = (loop_end<0) ? 0 : loop_end;
	howmany[layer] += increment;
  }

  /* Note: overlapped blits do not work with SDL, so we must go row by row! */
  //  printf(__FUNCTION__"[%d] loop: %d down to %d\n", layer, loop_start, loop_end);  
  for (y=loop_start; y>=loop_end; y--) {
	//	printf(__FUNCTION__"[%d] y=%d\n", layer, y);
	srcrect.w = surface->w - 2*map->bw;
	srcrect.h = map->bh;
	srcrect.x = 0;
	srcrect.y = y*map->bh;
	dstrect.x = 0;
	dstrect.y = (y+1)*map->bh;
	dstrect.w = surface->w - 2*map->bw;
	dstrect.h = map->bh;
	
#if !defined (CUSTOM_BLIT)
    SDL_FillRect(surface, &dstrect, SDL_MapRGB(surface->format, 0, 0, 0));
#endif
	DO_BLIT(surface, &srcrect, surface, &dstrect);
	/*
    SDL_FillRect(surface, &dstrect, SDL_MapRGB(surface->format, 0, 0, 0));
	if (SDL_BlitSurface(surface, &srcrect, surface, &dstrect) < 0)
	  fprintf(stderr, "BlitSurface error: %s\n", SDL_GetError());
	*/
  }


  if (howmany[layer] >= map->h) {
	howmany[layer] = 0;
	complete[layer] = 1;
	//	printf(__FUNCTION__ "[%d] COMPLETE\n", layer);
  }

  if (increment==-1) {
#if !defined (CUSTOM_BLIT)
	SDL_FillRect(LAYER(layer)->layer_bitmap, NULL, 
				 SDL_MapRGB(LAYER(layer)->layer_bitmap->format, 0, 0, 0));
#endif
	srcrect.x = 0;
	srcrect.y = 0;
	srcrect.w = surface->w;
	srcrect.h = surface->h;
	DO_BLIT(surface, &srcrect, LAYER(layer)->layer_bitmap, &srcrect);
	/*
	if (SDL_BlitSurface(surface, NULL, LAYER(layer)->layer_bitmap, NULL) < 0)
	  fprintf(stderr, "BlitSurface error: %s\n", SDL_GetError());
	*/
  }

}
#endif /* LAYER_DBL_BUFFER */

void update_map(int layer, int ux, int uy) 
{
  int y;
  SDL_Rect srcrect, dstrect;
  map_t *map = LAYER(layer)->map;
  SDL_Surface *surface = LAYER(layer)->layer_bitmap;

  /* Complete row buffer (i.e. row 0) */
  prepare_row_buffer(layer, -1);

  /* Scroll down rows */
#ifdef LAYER_DBL_BUFFER
  incremental_scroll(layer, -1);
#else
  for (y=map->h-1; y>=0; y--) {
	srcrect.w = surface->w - 2*map->bw;
	srcrect.h = map->bh;
	srcrect.x = 0;
	srcrect.y = y*map->bh;
	dstrect.x = 0;
	dstrect.y = (y+1)*map->bh;
	dstrect.w = surface->w - 2*map->bw;
	dstrect.h = map->bh;
	
#if !defined (CUSTOM_BLIT)
    SDL_FillRect(surface, &dstrect, SDL_MapRGB(surface->format, 0xff, 0, 0xff));
#endif
	DO_BLIT(surface, &srcrect, surface, &dstrect);
  }
#endif

  if (++LAYER(layer)->curr_y == map->h){
	LAYER(layer)->curr_y = 0;
  }
}

#endif /* LEVEL_MAP_PRECALC */

void scroll_map(int layer) {

  map_t *map = LAYER(layer)->map;
  SDL_Surface *screen = props.screen;
  SDL_Rect srcrect, dstrect;
  int dx = LAYER(layer)->horizontal_speed;
  int dy = LAYER(layer)->vertical_speed;

  static struct timeval t1,t2,t3;

  if (layer==0) {
	printf("WARNING: scroll_map() called for the background\n");
	return;
  }
  

  // XXX horizontal scrolling (dx) not supported

  //  printf("scroll_map() [scroll_cnt=%d]\n", LAYER(layer)->scroll_cnt);

  srcrect.x = 0;
#ifdef LEVEL_MAP_PRECALC
  /* XXX first part is constant, may be calculated only once */
  srcrect.y = (map->bh*map->h - SCREEN_HEIGHT) - LAYER(layer)->scroll_cnt;
#else
  srcrect.y = 2*map->bh - (LAYER(layer)->scroll_cnt % map->bh);
#endif
  srcrect.w = SCREEN_WIDTH;
  srcrect.h = SCREEN_HEIGHT;
  dstrect.x = 0; dstrect.y = 0;
  dstrect.w = SCREEN_WIDTH;
  dstrect.h = SCREEN_HEIGHT;

  gettimeofday(&t1, NULL);
  //  if (SDL_LowerBlit(LAYER(layer)->layer_bitmap, &srcrect, screen, &dstrect) < 0)
  if (SDL_BlitSurface(LAYER(layer)->layer_bitmap, &srcrect, screen, NULL) < 0)
	fprintf(stderr, "BlitSurface error [line %d]: %s\n", __LINE__, SDL_GetError());
  gettimeofday(&t2, NULL);
  timersub(&t2, &t1, &t3);
  printf("blit time %f msec\n", 1000*t3.tv_sec + (double)t3.tv_usec/1000);

#ifndef LEVEL_MAP_PRECALC
  if (LAYER(layer)->incr_update)
	prepare_row_buffer(layer, LAYER(layer)->incr_update);
#ifdef LAYER_DBL_BUFFER
  incremental_scroll(layer, map->h / (map->bw/dy));
#endif
#endif

  LAYER(layer)->scroll_cnt += dy;
#ifdef LEVEL_MAP_PRECALC
  if (LAYER(layer)->scroll_cnt >= map->bh*map->h) {
	LAYER(layer)->scroll_cnt = 0;
  }
#else
  if (LAYER(layer)->scroll_cnt >= map->bh) {
	LAYER(layer)->scroll_cnt -= map->bh;
	update_map(layer, 0, 1);
  }
#endif

}


#ifdef LEVEL_MAP_PRECALC
SDL_Surface *map_precalc(int layer) {

  int i,j;
  map_t *map = LAYER(layer)->map;
  SDL_Surface *temp, *surface;
  //  int fava;

  //  fava = map->h;
  //  map->h = 17;
  printf("trying to allocate a %dx%d surface...\n",
		 map->bw * map->w,
		 map->bh * map->h);
  
  temp = SDL_CreateRGBSurface(SDL_HWSURFACE, map->bw * map->w, map->bh * map->h,
							  32, rmask, gmask, bmask, 0);
  SDL_SetColorKey(temp, SDL_SRCCOLORKEY, SDL_MapRGB(temp->format, 0xff, 0, 0xff));
  surface = SDL_DisplayFormat(temp);
  SDL_FreeSurface(temp);
  SDL_FillRect(surface, NULL, 0xff00ff);


  for (j=0; j<map->h; j++) {
	printf(" map row %d\n", j);
	for (i=0; i<map->w; i++)
	  copy_block(layer, surface, i, map->data[j*map->w + i], map->h-1 - j);
  }

  //  map->h = fava;

  return surface;
}
#endif


/*inline int map_position() {
  return LAYER(1)->curr_y*LAYER(1)->map->bh + LAYER(1)->scroll_cnt;
}

inline int map_length() {
  return LAYER(1)->map->h*LAYER(1)->map->bh;
}*/

