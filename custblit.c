
#ifdef CUSTBLIT_USE_MEMCPY
#include <string.h>
#endif

#include <SDL/SDL.h>

#ifdef CUSTOM_BLIT

#ifdef CUSTBLIT_ASM
/*
 * Code borrowed and slightly modified from the Linux kernel (arch/i386/lib/mmx.c)
 */

struct i387_fxsave_struct {
        unsigned short  cwd;
        unsigned short  swd;
        unsigned short  twd;
        unsigned short  fop;
        long    fip;
        long    fcs;
        long    foo;
        long    fos;
        long    mxcsr;
        long    reserved;
        long    st_space[32];   /* 8*16 bytes for each FP-reg = 128 bytes */
        long    xmm_space[32];  /* 8*16 bytes for each XMM-reg = 128 bytes */
        long    padding[56];
} __attribute__ ((aligned (16))) fxsave;


void *_mmx_memcpy(void *to, const void *from, size_t len)
{
	void *p;
	int i;

	p = to;
	i = len >> 6; /* len/64 */

	asm volatile( "fxsave %0 ; fnclex": "=m" (fxsave));

	__asm__ __volatile__ (
#ifdef HAVE_PREFETCH
		"1: prefetch (%0)\n"		/* This set is 28 bytes */
		"   prefetch 64(%0)\n"
		"   prefetch 128(%0)\n"
		"   prefetch 192(%0)\n"
		"   prefetch 256(%0)\n"
#else
		"1:  \n"
#endif
		"2:  \n"
		".section .fixup, \"ax\"\n"
		"3: movw $0x1AEB, 1b\n"	/* jmp on 26 bytes */
		"   jmp 2b\n"
		".previous\n"
		".section __ex_table,\"a\"\n"
		"	.align 4\n"
		"	.long 1b, 3b\n"
		".previous"
		: : "r" (from) );
		
	
	for(; i>0; i--)
	{
		__asm__ __volatile__ (
#ifdef HAVE_PREFETCH
		"1:  prefetch 320(%0)\n"
#else
		"1:  \n"
#endif
		"2:  movq (%0), %%mm0\n"
		"  movq 8(%0), %%mm1\n"
		"  movq 16(%0), %%mm2\n"
		"  movq 24(%0), %%mm3\n"
		"  movq %%mm0, (%1)\n"
		"  movq %%mm1, 8(%1)\n"
		"  movq %%mm2, 16(%1)\n"
		"  movq %%mm3, 24(%1)\n"
		"  movq 32(%0), %%mm0\n"
		"  movq 40(%0), %%mm1\n"
		"  movq 48(%0), %%mm2\n"
		"  movq 56(%0), %%mm3\n"
		"  movq %%mm0, 32(%1)\n"
		"  movq %%mm1, 40(%1)\n"
		"  movq %%mm2, 48(%1)\n"
		"  movq %%mm3, 56(%1)\n"
		".section .fixup, \"ax\"\n"
		"3: movw $0x05EB, 1b\n"	/* jmp on 5 bytes */
		"   jmp 2b\n"
		".previous\n"
		".section __ex_table,\"a\"\n"
		"	.align 4\n"
		"	.long 1b, 3b\n"
		".previous"
		: : "r" (from), "r" (to) : "memory");
		from+=64;
		to+=64;
	}
	/*
	 *	Now do the tail of the block
	 */
	memcpy(to, from, len&63);

	asm volatile( "fxrstor %0"
				  : : "m" (fxsave) );

	return p;
}

#define _memcpy _mmx_memcpy

#else  /* CUSTBLIT_ASM */

#define _memcpy memcpy

#endif /* CUSTBLIT_ASM */

/* Assumptions:
 *  - the given x,y/w,h parameters are compatible (i.e. less than)
 *    with the specified target surface
 *  - no clipping is supported
 *  - surface depth is 32 bits
 *  - surface width is multiple of 4, as well as surface pitch
 */
void cust_BlitSurface_opaque(SDL_Surface *src, int sx, int sy, int w, int h,
							 SDL_Surface *dst, int dx, int dy) {

  int i,j;
  unsigned int *lsrc, *ldest;
  unsigned char *source = src->pixels, *dest = dst->pixels;
#ifdef CUSTBLIT_USE_MEMCPY
  int w4 = 4*w;
  unsigned int lspitch = src->pitch>>2, ldpitch = dst->pitch>>2;
#else
  unsigned int lspitch = (src->pitch>>2) - w,
	ldpitch = (dst->pitch>>2) - w;
#endif

  lsrc = ((unsigned int *)source + (sx>>2) + (sy*src->pitch>>2));
  ldest = ((unsigned int *)dest + (dx>>2) + (dy*dst->pitch>>2));

  if (SDL_MUSTLOCK(src)) {
	if (SDL_LockSurface(src) < 0)
	  fprintf(stderr, "Can't lock screen: %s\n", SDL_GetError());
  }
  if (SDL_MUSTLOCK(dst)) {
	if (SDL_LockSurface(dst) < 0)
	  fprintf(stderr, "Can't lock screen: %s\n", SDL_GetError());
  }

  for (i=0; i<h; i++) {
#ifdef CUSTBLIT_USE_MEMCPY
	_memcpy(ldest, lsrc, w4);
#else
	for (j=0; j<w; j++)
	  *ldest++ = *lsrc++;
#endif
	ldest += ldpitch;
	lsrc += lspitch;
  }

  if (SDL_MUSTLOCK(dst)) {
	SDL_UnlockSurface(dst);
  }
  if (SDL_MUSTLOCK(src)) {
	SDL_UnlockSurface(src);
  }
  
}



#if defined (CUSTBLIT_CONTIGUOS) && defined (CUSTBLIT_USE_MEMCPY)
/* Assumptions:
 *  - source and destination surfaces are contiguos (i.e. pitch==w)
 *  - the given x,y/w,h parameters are compatible (i.e. less than)
 *    with the specified target surface
 *  - no clipping is supported
 *  - surface depth is 32 bits
 *  - surface width is multiple of 4, as well as surface pitch
 */
void cust_BlitSurface_opaque_cont(SDL_Surface *src, int sx, int sy, int w, int h,
								  SDL_Surface *dst, int dx, int dy) {

  int i,j;
  unsigned int *lsrc, *ldest;
  unsigned char *source = src->pixels, *dest = dst->pixels;
  int w4 = 4*w;

  /* These MUST be true:
	 assert(src->pitch == w4);
	 assert(dst->pitch == w4);
  */
  
  lsrc = ((unsigned int *)source + (sx>>2) + (sy*src->pitch>>2));
  ldest = ((unsigned int *)dest + (dx>>2) + (dy*dst->pitch>>2));

  if (SDL_MUSTLOCK(src)) {
	if (SDL_LockSurface(src) < 0)
	  fprintf(stderr, "Can't lock screen: %s\n", SDL_GetError());
  }
  if (SDL_MUSTLOCK(dst)) {
	if (SDL_LockSurface(dst) < 0)
	  fprintf(stderr, "Can't lock screen: %s\n", SDL_GetError());
  }

  _memcpy(ldest, lsrc, w4*h);

  if (SDL_MUSTLOCK(dst)) {
	SDL_UnlockSurface(dst);
  }
  if (SDL_MUSTLOCK(src)) {
	SDL_UnlockSurface(src);
  }
  
}
#else
#define cust_BlitSurface_opaque_cont cust_BlitSurface_opaque
#endif /* CUSTBLIT_CONTIGUOS && CUSTBLIT_USE_MEMCPY */


void cust_BlitSurface_trans(SDL_Surface *src, int sx, int sy, int w, int h,
							SDL_Surface *dst, int dx, int dy) {

  int i,j;
  unsigned int *lsrc, *ldest;
  unsigned char *source = src->pixels, *dest = dst->pixels;
  unsigned int lspitch = (src->pitch>>2) - w,
	ldpitch = (dst->pitch>>2) - w;
  
  lsrc = ((unsigned int *)source + (sx>>2) + (sy*src->pitch>>2));
  ldest = ((unsigned int *)dest + (dx>>2) + (dy*dst->pitch>>2));

  if (SDL_MUSTLOCK(src)) {
	if (SDL_LockSurface(src) < 0)
	  fprintf(stderr, "Can't lock screen: %s\n", SDL_GetError());
  }
  if (SDL_MUSTLOCK(dst)) {
	if (SDL_LockSurface(dst) < 0)
	  fprintf(stderr, "Can't lock screen: %s\n", SDL_GetError());
  }

  for (i=0; i<h; i++) {
	for (j=0; j<w; j++)
	  if (*lsrc)
		*ldest++ = *lsrc++;
	ldest += ldpitch;
	lsrc += lspitch;
  }

  if (SDL_MUSTLOCK(dst)) {
	SDL_UnlockSurface(dst);
  }
  if (SDL_MUSTLOCK(src)) {
	SDL_UnlockSurface(src);
  }
  
}


#endif /* CUSTOM_BLIT */
