#include "objects.h"
#include "animations.h"
#include "datafiles.h"

static int anim_is_loaded[ANIM_MAX];
static anim_seq_t *animations[ANIM_MAX];

static struct {
  anim_type_t	type;
  const char	*filename;
  int			width;
  int			height;
  int			num_frames;
  int			rows;
  int			cols;
  int			speed;
} anim_resources[] = {
  { ANIM_EXPLOSION_32,   FILE_ANIM_EXPLOSION_32, 32, 32, 10, 2, 5, 1 },
  { ANIM_EXPLOSION_64,   FILE_ANIM_EXPLOSION_64, 64, 64, 10, 2, 5, 1 },
  { ANIM_ENEMY_DRONE,    FILE_ANIM_ENEMY_DRONE,  32, 32, 16, 2, 8, 2 },
  { ANIM_ENEMY_RUSHER,   FILE_ANIM_ENEMY_RUSHER, 64, 32, 24, 6, 4, 2 },
  { ANIM_ASTEROIDG_32,   FILE_ANIM_ASTEROIDG_32, 32, 32, 16, 2, 8, 2 },
  { ANIM_ASTEROIDG_64,   FILE_ANIM_ASTEROIDG_64, 64, 64, 24, 3, 8, 3 },
  { ANIM_ASTEROIDG_EXPL, FILE_ANIM_ASTEROIDG_EXPL, 8, 8, 4, 1, 4, 2 },
  { ANIM_HERO,   		 FILE_ANIM_HERO,         64, 64, 7, 1, 7, 2 },
  { ANIM_HERO_MISSILE,   FILE_ANIM_HERO_MISSILE, 16, 16, 6, 3, 2, 2 },
  { ANIM_HERO_HMISSILE,  FILE_ANIM_HERO_HMISSILE, 32, 32, 8, 2, 4, -1 },
  { ANIM_BONUS_DIVE,	 FILE_ANIM_BONUS_DIVE,    32, 32, 8, 2, 4, 2 },
  { ANIM_BONUS_INVULN,	 FILE_ANIM_BONUS_INVULN,  32, 32, 8, 2, 4, 2 },
  { ANIM_BONUS_MISSILE,  FILE_ANIM_BONUS_MISSILE, 32, 32, 8, 2, 4, 2 },
  { ANIM_BONUS_SCORE,	 FILE_ANIM_BONUS_SCORE,	32, 32, 8, 2, 4, 2 },
  { ANIM_BONUS_SHIELD,	 FILE_ANIM_BONUS_SHIELD,	32, 32, 8, 2, 4, 2 },
  { ANIM_BONUS_SPEED,	 FILE_ANIM_BONUS_SPEED,	32, 32, 8, 2, 4, 2 },
  { ANIM_BONUS_WEAPON,   FILE_ANIM_BONUS_WEAPON,  32, 32, 8, 2, 4, 2 },
  { ANIM_MAX, NULL, 0, 0, 0, 0, 0, 0}
};
  

anim_seq_t *get_animation(anim_type_t type) {
  if (anim_is_loaded[type])
	return animations[type];

  fprintf(stderr, "Run-time error: animation %d not loaded\n", type);
  return NULL;

}

/* Loads 'num' frames (w*h) from a bmp file */
anim_seq_t *load_animation(anim_type_t type, const char *file_name,
						   int w, int h, int num, 
						   int rows, int cols, int spd) {
  
  int i, colctr = 0, rowctr = 0;
  SDL_Surface *dummy, *global;
  unsigned char *pixmap;

  if (anim_is_loaded[type])
	return animations[type];

  animations[type] = (anim_seq_t *)malloc(sizeof(anim_seq_t));
  animations[type]->n = num;
  animations[type]->speed = spd;

  dummy = SDL_LoadBMP(file_name);
  SDL_SetColorKey(dummy, SDL_SRCCOLORKEY, SDL_MapRGB(dummy->format, 0xff, 0, 0xff));
  global = SDL_DisplayFormat(dummy);
  SDL_FreeSurface(dummy);

  pixmap = global->pixels;
  for (i=0; i<num; i++) {
	dummy = SDL_CreateRGBSurfaceFrom(pixmap, w, h, 32, global->pitch,
									 rmask, gmask, bmask, amask);
	SDL_SetColorKey(dummy, SDL_SRCCOLORKEY, SDL_MapRGB(dummy->format, 0xff, 0, 0xff));
	animations[type]->surface[i] = SDL_DisplayFormat(dummy);
	SDL_FreeSurface(dummy);
	if (++colctr == cols) {
	  colctr = 0;
	  if (++rowctr == rows && i!=(num-1)) {
		fprintf(stderr, "Warning: wrong frame number %d\n", num);
	  }
	  pixmap = global->pixels + global->pitch*h*rowctr;
	} else {
	  pixmap += w*4;				/* assuming 32 bits surfaces */
	}
  }

  SDL_FreeSurface(global);

  anim_is_loaded[type] = 1;

  return animations[type];
}


void init_animations() {
  int cnt=0;

  while (anim_resources[cnt].filename) {
	load_animation(anim_resources[cnt].type,
				   anim_resources[cnt].filename,
				   anim_resources[cnt].width,
				   anim_resources[cnt].height,
				   anim_resources[cnt].num_frames,
				   anim_resources[cnt].rows,
				   anim_resources[cnt].cols,
				   anim_resources[cnt].speed);
	cnt++;
  }

}

void free_animations() {
  int cnt=0;

  while (anim_resources[cnt].filename) {
	// free_animation(...); TBD
	cnt++;
  }
}

