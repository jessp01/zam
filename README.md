# ZAM README (C) by Jessex

**Original porting by Gianluca Insolvibile (branzo at sourceforge.net): http://phavon.sourceforge.net**

ZAM is a clone of the old times Amiga classic game "Xenon 2: The megablast",
originally produced by [The Bitmap Brothers](https://www.bitmap-brothers.co.uk). 
Please note that the original games Xenon, Xenon 2 and Xenon 2K are (C) The Bitmap Brothers.

The code for the Linux version has been rewritten completely from scratch and is currently based on graphics and sounds borrowed from [Xenon 2000: Project PCF](https://archive.org/details/Xenon_2000_Project_PCF).

[![img](zam.png)]

## Requirements

In order to compile the game, you need the following libraries:

- SDL
- SDL_mixer
- smpeg

You can find all of them at http://www.libsdl.org. 
Also, you must be able to run X in 32 bpp mode.

In order to run the game, you will need to download the Xenon 2000 Project PCF Windows executable [here](https://archive.org/details/Xenon_2000_Project_PCF)

## Installation

- Run `Xenon2000.exe` with WINE to install it. 
This will create a directory like `~/.wine/c_drive/Program Files/The Bitmap Brothers/Xenon 2000 - Project PCF`

- Copy (or link) the following directories into ZAM's main directory:
```
$ cd ZAM
$ ln -s <path to Xenon II PCF>/graphics/24bit gfx
$ ln -s <path to Xenon II PCF>/sounds .
$ ln -s <path to Xenon II PCF>/music .
```

- `$ make all`

Finally, run the game like this:

`$ ./ZAM -audio 3 -background -full`

## Thanks

- Bitmap Brothers for letting me borrow their graphics and admit it publicly ;-)
- Gianluca Insolvibile (branzo at sourceforge.net) for the original porting work
