#include <math.h>

#include "zam.h"
#include "objects.h"
#include "animations.h"
#include "constants.h"
#include "datafiles.h"
#include "profile.h"

object_t *enemies[MAX_ENEMIES];
int enemy_score(int type);
void move_enemy(int i);

/* Score earned for each type of enemy */
static int enemy_reward[ENEMY_NUMBER] = {
  2000,	/* ENEMY_DRONE */
  5000,	/* ENEMY_RUSHER */
  1000,	/* ENEMY_ASTEROID32 */
  1000,	/* ENEMY_ASTEROID64 */
};

#define SINE_TABLE_LENGTH 128
static int sine_table[SINE_TABLE_LENGTH];

#define EN(i) enemies[i]
#define EXPLOSION(i) EN(i)->expl_bmap
#define SPR(i) EN(i)->sprite
object_t *find_nearest_enemy(int x, int y) ;
void calc_sine_table() {
  int i;
  for (i=0; i<SINE_TABLE_LENGTH; i++) {
	sine_table[i] = 5*sin(2*M_PI*(double)i/SINE_TABLE_LENGTH);
	if (sine_table[i]==0)
	  sine_table[i]=1;
  }

}

inline
void move_enemy(int i) {
  switch (EN(i)->type) {
	
	case ENEMY_DRONE:
	  /* For movement on the x-axis, spdx is used as an index
	   * into the sine table */
	  SPR(i)->x += sine_table[SPR(i)->spdx++];
	  if (SPR(i)->x<0) { 
		SPR(i)->x = 0;
		SPR(i)->spdx += SINE_TABLE_LENGTH/2;
	  }
	  else if ((SPR(i)->x > SPR(i)->surf->w - SPR(i)->w)) {
		SPR(i)->x = SPR(i)->surf->w - SPR(i)->w;
		SPR(i)->spdx += SINE_TABLE_LENGTH/2;
	  }
	  if (SPR(i)->spdx >= SINE_TABLE_LENGTH)
		SPR(i)->spdx -= SINE_TABLE_LENGTH;
	  
	  /* Y axis movement */
	  SPR(i)->y += SPR(i)->spdy;
	  if ((SPR(i)->y<0 && SPR(i)->spdy<=0) || 
		  (SPR(i)->y > SPR(i)->surf->h - SPR(i)->h)) {
		EN(i)->alive=0;
	  }
	  break;
	  
	case ENEMY_RUSHER:
	case ENEMY_ASTEROID32:
	case ENEMY_ASTEROID64:
	  /* X axis movement */
	  SPR(i)->x += SPR(i)->spdx;
	  if ((SPR(i)->x<0 && SPR(i)->spdx<=0) || 
		  (SPR(i)->x > SPR(i)->surf->w - SPR(i)->w)) {
		EN(i)->alive=0;
	  }
	  /* Y axis movement */
	  SPR(i)->y += SPR(i)->spdy;
	  if ((SPR(i)->y<0 && SPR(i)->spdy<=0) || 
		  (SPR(i)->y > SPR(i)->surf->h - SPR(i)->h)) {
		EN(i)->alive=0;
	  }
	  break;
	  
	default:
	  fprintf(stderr, "Unknown enemy type %d\n", EN(i)->type);
  }

}

void update_enemies() {
  int i;
  
  for (i=0; i<MAX_ENEMIES; i++) {
	if (EN(i)->alive) {
	  if (EN(i)->is_dying) {
		SPR(i)->image = EXPLOSION(i)->surface[EN(i)->anim_frame];
		if (--EN(i)->anim_delay==0) {
		  EN(i)->anim_delay = EXPLOSION(i)->speed;
		  if (++EN(i)->anim_frame == EXPLOSION(i)->n) {
			EN(i)->alive = 0;
			EN(i)->is_dying = 0;
		  }
		}
	  } else {
		move_enemy(i);
		/* Roll animation */
		if (EN(i)->anim_bmap) {
		  SPR(i)->image = EN(i)->anim_bmap->surface[EN(i)->anim_frame];
		  if (--EN(i)->anim_delay == 0) {
			EN(i)->anim_delay = EN(i)->anim_bmap->speed;
			if (++EN(i)->anim_frame == EN(i)->anim_bmap->n)
			  EN(i)->anim_frame = 0;
		  }
		}
	  }
	} /* if (EN(i)->alive) */
  }

}

void draw_enemies() {
  int i;

  for (i=0; i<MAX_ENEMIES; i++)
	if (EN(i)->alive)
	  sprite_draw(SPR(i));
}

void reset_enemies() {
  int i;

  for (i=0; i<MAX_ENEMIES; i++) {
	EN(i)->alive = 0;
	EN(i)->is_dying = 0;
	EN(i)->energy = 1;
	EN(i)->anim_bmap = NULL;
	EN(i)->expl_bmap = NULL;
	EN(i)->anim_frame = 0;
	SPR(i)->spdx = (i%2)?0:SINE_TABLE_LENGTH/2;
	SPR(i)->spdy = 0;
  }
}

void init_enemies(SDL_Surface *screen) {
  int i;

  calc_sine_table();
  for (i=0; i<MAX_ENEMIES; i++) {
	EN(i) = (object_t *)malloc(sizeof(object_t));
	EN(i)->family = FAM_ENEMY;
	SPR(i) = sprite_init_empty(32, 32, screen);
  }
  reset_enemies();

}

void new_enemy(enemy_type_t type, int x, int y, int spdx, int spdy) {
  int i;

  for (i=0; i<MAX_ENEMIES; i++) {
	if (!EN(i)->alive && !EN(i)->is_dying) {
	  EN(i)->type = type;
	  EN(i)->anim_frame = 0;
	  //	  printf(__FUNCTION__ " type %d\n", type);
	  switch (type) {
	  case ENEMY_DRONE:
		EN(i)->anim_bmap = get_animation(ANIM_ENEMY_DRONE);
		EN(i)->expl_bmap = get_animation(ANIM_EXPLOSION_32);
		EN(i)->energy = 1;
		break;
	  case ENEMY_RUSHER:
		EN(i)->anim_bmap = get_animation(ANIM_ENEMY_RUSHER);
		EN(i)->expl_bmap = get_animation(ANIM_EXPLOSION_64);
		EN(i)->energy = 5;
		break;
	  case ENEMY_ASTEROID32:
		EN(i)->anim_bmap = get_animation(ANIM_ASTEROIDG_32);
		EN(i)->expl_bmap = get_animation(ANIM_ASTEROIDG_EXPL);
		EN(i)->energy = 1;
		break;
	  case ENEMY_ASTEROID64:
		EN(i)->anim_bmap = get_animation(ANIM_ASTEROIDG_64);
		EN(i)->expl_bmap = get_animation(ANIM_ASTEROIDG_EXPL);
		EN(i)->energy = 1;
		break;
	  default:
		fprintf(stderr, "Unknown enemy type %d\n", type);
	  }
	  if (EN(i)->anim_bmap) {
		EN(i)->anim_delay = EN(i)->anim_bmap->speed;
		SPR(i)->w = EN(i)->anim_bmap->surface[0]->w;
		SPR(i)->h = EN(i)->anim_bmap->surface[0]->h;
	  }
	  EN(i)->alive = 1;
	  EN(i)->is_dying = 0;
	  SPR(i)->spdx = spdx;
	  SPR(i)->spdy = spdy;
	  sprite_setpos(SPR(i), x, y);
	  return;
	}
  }
  fprintf(stderr, "No more enemies available!\n");

}


//#define MAX_DISTANCE 640*640+320*320 ????
#define MAX_DISTANCE 640*640+480*480
/* XXX is abs() faster than multiplying ? */
#define DISTANCE(x1,y1,x2,y2) ( ((x1-x2)*(x1-x2))+((y1-y2)*(y1-y2)) )

inline
object_t *find_nearest_enemy(int x, int y) {

  /* Can we find a smartest way than scanning all the enemies? 
   * Anyway, with 30 enemies this function takes something
   * between 2 and 15 usecs to run ... in the overall fps budget
   * it's not so much!
   */

  int i, dist, min_dist = MAX_DISTANCE;
  object_t *nearest = NULL;

  //  PROFILE_START;

  /* XXX we must exclude out-of-screen enemies! */
  for (i=0; i<MAX_ENEMIES; i++)
	if (EN(i)->alive && !EN(i)->is_dying)
	  if ((dist=DISTANCE(SPR(i)->x, SPR(i)->y, x, y)) < min_dist) {
		min_dist = dist;
		nearest = EN(i);
	  }

  //  printf(__FUNCTION__ " nearest: %p\n", nearest);

  //  PROFILE_END;
  return nearest;
}

inline int enemy_score(int type) {

  assert(type < ENEMY_NUMBER);

  return enemy_reward[type];
}
