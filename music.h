/***************************************************************************
 * ZAM                                                                     *
 * (C) 2022 Jessex                                                         *
 *                                                                         *
 * Original porting from Xenon2000: Phavon                                 *
 * (C) 2002 Gianluca Insolvibile (branzo@users.sourceforge.net)            *
 * http://phavon.sourceforge.net/                                          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 ***************************************************************************/

