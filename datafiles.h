
#ifndef __DATAFILES_H
#define __DATAFILES_H

#define FILE_BACKGROUND	"gfx/galaxy2.bmp"
#define FILE_ANIM_HERO	"gfx/Ship1.bmp"
#define FILE_LIFE_HERO	"gfx/PULife.bmp"

#define FILE_FONT8		"gfx/Font8x8.bmp"
#define FILE_FONT16		"gfx/font16x16.bmp"

#define FILE_ANIM_EXPLOSION_32	"gfx/explode32.bmp"
#define FILE_ANIM_EXPLOSION_64	"gfx/explode64.bmp"
#define FILE_ANIM_ENEMY_DRONE	"gfx/drone.bmp"
#define FILE_ANIM_ENEMY_RUSHER	"gfx/rusher.bmp"
#define FILE_ANIM_ASTEROIDG_32	"gfx/GAster32.bmp"
#define FILE_ANIM_ASTEROIDG_64	"gfx/GAster64.bmp"
#define FILE_ANIM_ASTEROIDG_EXPL "gfx/GDust.bmp"
#define FILE_ANIM_HERO_MISSILE	"gfx/missile.bmp"
#define FILE_ANIM_HERO_HMISSILE	"gfx/hmissile.bmp"

#define FILE_ANIM_BONUS_DIVE	"gfx/PUDive.bmp"
#define FILE_ANIM_BONUS_INVULN	"gfx/PUInvuln.bmp"
#define FILE_ANIM_BONUS_MISSILE	"gfx/PUMissil.bmp"
#define FILE_ANIM_BONUS_SCORE	"gfx/PUScore.bmp"
#define FILE_ANIM_BONUS_SHIELD	"gfx/PUShield.bmp"
#define FILE_ANIM_BONUS_SPEED	"gfx/PUSpeed.bmp"
#define FILE_ANIM_BONUS_WEAPON	"gfx/PUWeapon.bmp"

#define FILE_MUSIC_SOUNDTRACK	"music/game.mp3"

#define FILE_SOUND_MISSILE		"sounds/fire_missile.wav"
#define FILE_SOUND_SMALLEXPL	"sounds/small_explosion.wav"
#define FILE_SOUND_HEROEXPL		"sounds/player_destroyed.wav"

#endif /* __DATAFILES_H */
