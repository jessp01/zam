
#include <stdlib.h>

#include "objects.h"
#include "sprite.h"

#define PIXEL(pix,x,y,p) *(Uint32 *)((pix) + (y)*(p) + (x)*4)
#define KEY 0x00ff00ff

sprite_t *sprite_init(const char *name, SDL_Surface *surface) {

  SDL_Surface *tmp;
  sprite_t *spr = (sprite_t *)malloc(sizeof(sprite_t));

  if (spr) {
	tmp = SDL_LoadBMP(name);
	SDL_SetColorKey(tmp, SDL_SRCCOLORKEY, 
					SDL_MapRGB(tmp->format, 0xff, 0x00, 0xff));
	spr->image = SDL_DisplayFormat(tmp);
	SDL_FreeSurface(tmp);
	
	spr->surf = surface;
	spr->w = spr->image->w;
	spr->h = spr->image->h;
  }

  return spr;
}

sprite_t *sprite_init_empty(int w, int h, SDL_Surface *surface) {

  SDL_Surface *tmp;
  sprite_t *spr = (sprite_t *)malloc(sizeof(sprite_t));

  if (spr) {
	tmp = SDL_CreateRGBSurface(SDL_SWSURFACE, w, h,
							   32, rmask, gmask, bmask, amask);
	SDL_SetColorKey(tmp, SDL_SRCCOLORKEY, 
					SDL_MapRGB(tmp->format, 0xff, 0x00, 0xff));
	spr->image = SDL_DisplayFormat(tmp);
	SDL_FreeSurface(tmp);
	
	spr->surf = surface;
	spr->w = spr->image->w;
	spr->h = spr->image->h;
  }

  return spr;
}


inline
void sprite_setpos(sprite_t *s, int x, int y) {
  s->x = x;
  s->y = y;
}


inline
int sprite_draw(sprite_t *s) {

  SDL_Rect target;

  target.x = s->x;
  target.y = s->y;
  SDL_BlitSurface(s->image, NULL, s->surf, &target);
}


/*
 * Warning: works only with 32 bits surfaces!
 */
int sprite_collide(sprite_t *s1, sprite_t *s2) {

  int i,j;
  int x1,y1,w1,h1;
  int x2,y2,w2,h2;
  unsigned char *pix1, *pix2;
  int p1, p2;
  int ow, oh;
  int x1s, y1s, x2s, y2s;

  x1 = s1->x;
  x2 = s2->x;
  y1 = s1->y;
  y2 = s2->y;
  w1 = s1->w; h1 = s1->h;
  w2 = s2->w; h2 = s2->h;
  p1 = s1->image->pitch;
  p2 = s2->image->pitch;
  pix1 = s1->image->pixels;
  pix2 = s2->image->pixels;

  if ( (x2+w2<x1) || (x2>x1+w1) ||
	   (y2+h2<y1) || (y2>y1+h1) )
	return 0;

  /* Check overlapping region for non-transparent pixels */
  if (x2<x1) {
	ow = w2 - (x1-x2);
	x1s = 0;
	x2s = w2-ow;
  } else {
	ow = w1 - (x2-x1);
	x1s = w1-ow;
	x2s = 0;
  }
  if (y2<y1) {
	oh = h2 - (y1-y2);
	y1s = 0;
	y2s = h2-oh;
  } else {
	oh = h1 - (y2-y1);
	y1s = h1-oh;
	y2s = 0;
  }

  for (i=0; i<oh; i++)
	for (j=0; j<ow; j++)
	  if ( (PIXEL(pix1, x1s+j, y1s+i, p1) != KEY) &&
		   (PIXEL(pix2, x2s+j, y2s+i, p2) != KEY) )
		return 1;

  return 0;

}
