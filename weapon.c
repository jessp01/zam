
#include <math.h>

#include "sprite.h"
#include "constants.h"
#include "objects.h"
#include "profile.h"

object_t *bullets[MAX_BULLETS];

/* Damage inflicted by each kind of bullet */
static int bullet_damage[BULLETS_NUMBER] = {
  1,	/* BULLET_HERO_MISSILE1 */
  2,	/* BULLET_HERO_MISSILE2 */
  3,	/* BULLET_HERO_MISSILE3 */
  2,	/* BULLET_HERO_HMISSILE */
};

#define B(i) bullets[i]
#define SPR(i) B(i)->sprite

/* from enemy.c */
object_t *find_nearest_enemy(int x, int y);

void precalc_hmissile_table();
void update_hmissile(object_t *missile);
int hmissile_frame(int spdx, int spdy);

void reset_bullets() {

  int i;

  for (i=0; i<MAX_BULLETS; i++) {
	B(i)->family = FAM_BULLET;
	B(i)->alive = 0;
	B(i)->is_dying = 0;
	B(i)->energy = 1;
	B(i)->anim_bmap = NULL;
	B(i)->expl_bmap = NULL;
	B(i)->anim_frame = 0;
  }

}

void init_bullets(SDL_Surface *screen) {

  int i,j;
  unsigned char *buffer;
  SDL_Surface *tmpsurf;

  precalc_hmissile_table();

  for (i=0; i<MAX_BULLETS; i++) {
	B(i) = (object_t *)malloc(sizeof(object_t));
	SPR(i) = sprite_init_empty(8, 8, screen);
  }
  reset_bullets();

}

void update_bullets() {

  int i;
  int sf, ef;

  for (i=0; i<MAX_BULLETS; i++) {
	if (B(i)->alive) {
	  SPR(i)->x += SPR(i)->spdx;
	  SPR(i)->y += SPR(i)->spdy;
	  if (SPR(i)->x < 0 || SPR(i)->x > SPR(i)->surf->w ||
		  SPR(i)->y < 0 || SPR(i)->y > SPR(i)->surf->h)
		B(i)->alive = 0;
	  /* Animate */
	  if (B(i)->anim_bmap) {
		switch (B(i)->type) {
		case BULLET_HERO_MISSILE1:
		  sf = 0; ef = 1;
		  break;
		case BULLET_HERO_MISSILE2:
		  sf = 2; ef = 3;
		  break;
		case BULLET_HERO_MISSILE3:
		  sf = 4; ef = 5;
		  break;
		case BULLET_HERO_HMISSILE:
		  update_hmissile(B(i));
		  break;
		default:
		  sf = 0; ef = B(i)->anim_bmap->n;
		}
		SPR(i)->image = B(i)->anim_bmap->surface[B(i)->anim_frame];
		if (--B(i)->anim_delay==0) {
		  B(i)->anim_delay = B(i)->anim_bmap->speed;
		  if (++B(i)->anim_frame == ef) {
			B(i)->anim_frame = sf;
		  }
		}
	  } /* if exists animation */
	}
  }

}

void draw_bullets() {

  int i;

  for (i=0; i<MAX_BULLETS; i++)
	if (B(i)->alive)
	  sprite_draw(SPR(i));

}

void new_bullet(int type, int x, int y) {

  int i;

  for (i=0; i<MAX_BULLETS; i++)
	if (!B(i)->alive) {
	  B(i)->type = type;
	  B(i)->anim_bmap = get_animation(ANIM_HERO_MISSILE);
	  SPR(i)->w = 8;
	  SPR(i)->h = 8;
	  switch (B(i)->type) {
	  case BULLET_HERO_MISSILE1:
		B(i)->anim_frame = 0;
		SPR(i)->spdx = 0;
		SPR(i)->spdy = -20;
		break;
	  case BULLET_HERO_MISSILE2:
		B(i)->anim_frame = 2;
		SPR(i)->spdx = 0;
		SPR(i)->spdy = -20;
		break;
	  case BULLET_HERO_MISSILE3:
		B(i)->anim_frame = 4;
		SPR(i)->spdx = 0;
		SPR(i)->spdy = -20;
		break;
	  case BULLET_HERO_HMISSILE:
		B(i)->anim_bmap = get_animation(ANIM_HERO_HMISSILE);
		SPR(i)->w = 16;
		SPR(i)->h = 16;
		B(i)->aux.target = find_nearest_enemy(x,y);
		B(i)->dont_update = 0;
		sprite_setpos(SPR(i), x, y);
		update_hmissile(B(i));
		break;
	  default:
		B(i)->anim_frame = 0;
	  }
	  if (B(i)->anim_bmap) {
		B(i)->anim_delay = B(i)->anim_bmap->speed;
		SPR(i)->image = B(i)->anim_bmap->surface[0];
	  }
	  B(i)->alive = 1;
	  B(i)->is_dying = 0;
	  B(i)->energy = 1;
	  sprite_setpos(SPR(i), x, y);

	  sound_shot();
	  return;
	}
  
}

int is_hit(sprite_t *obj) {
  int i,ret=0;

  for (i=0; i<MAX_BULLETS; i++)
	if (B(i)->alive)
	  if (sprite_collide(obj, SPR(i))) {
		B(i)->alive = 0;
		ret += bullet_damage[B(i)->type];
	  }

  return ret;
}


/********************************
 *                              *
 *     Homing missile stuff     *
 *                              *
 ********************************/

#define isign(x) (((x)>0)?1:-1)
#define zsign(x) ((x)?(isign(x)):0)
inline
int hmissile_frame(int spdx, int spdy) {
  static char frames[16] = {
	7,	/* 0000 */
	6,	/* 0001 */
	5,	/* 0010 */
	-1,	/* 0011 */
	0,	/* 0100 */
	0,	/* 0101 */
	4,	/* 0110 */
	-1,	/* 0111 */
	1,	/* 1000 */
	2,	/* 1001 */
	3,	/* 1010 */
	-1,	/* 1011 */
	-1,	/* 1100 */
	-1,	/* 1101 */
	-1,	/* 1110 */
	-1,	/* 1111 */
  };
  int idx, x_idx, y_idx;

  if (abs(spdx)<5) spdx = 0;
  if (abs(spdy)<5) spdy = 0;
  x_idx = zsign(spdx);
  y_idx = zsign(spdy);

  //  printf(__FUNCTION__ " frame is %d\n",
  //		 frames[((x_idx+1)<<2) + (y_idx+1)]);

  // TEMP
  if (frames[((x_idx+1)<<2) + (y_idx+1)] == -1)
	printf(" PORKO CAZZO! spdx,spdy=%d,%d -> idx=%d\n",
		   spdx, spdy,((x_idx+1)<<2) + (y_idx+1) );

  return frames[((x_idx+1)<<2) + (y_idx+1)];
  
}

#define HMISSILE_SPEED 10		/* XXX to be moved elsewhere */
#define DISTANCE(x1,y1,x2,y2) ( ((x1-x2)*(x1-x2))+((y1-y2)*(y1-y2)) )
#define HMISSILE_SPD_X(dx,dy) (hmissile_table[dx][dy] & 0x0f)
#define HMISSILE_SPD_Y(dx,dy) ((hmissile_table[dx][dy] & 0xf0)>>4)

static unsigned char hmissile_table[640][480];

void precalc_hmissile_table() {

  int dx, dy;
  double a, f;
  int mx=0,my=0;

  for (dx=0; dx<640; dx++)
	for (dy=0; dy<480; dy++) {
	  a = (double)dy/(double)dx;
	  f = (double)(HMISSILE_SPEED*HMISSILE_SPEED)/(1+a*a);
	  hmissile_table[dx][dy] = (zsign(dx)*(int)sqrt(f) +
								((zsign(dy)*(int)(sqrt(a*a*f)))<<4));
	}
}


/* Set spdx/spdy and anim_frame of an homing missile
 * depending on target's position.                   */
void update_hmissile(object_t *missile) {

  object_t *target = missile->aux.target;
  int dx,dy;
  unsigned int adx, ady;
  int spd = HMISSILE_SPEED;

  //  PROFILE_START;

  if (!target) {
	missile->sprite->spdx = 0;
	missile->sprite->spdy = -15;
	missile->anim_frame = hmissile_frame(0, -15);
	return;
  }

  if (!target->alive || target->is_dying) {
	/* Retarget missile */
	missile->aux.target = find_nearest_enemy(missile->sprite->x,
											 missile->sprite->y);
	return;
  }

  dx = target->sprite->x - missile->sprite->x;
  dy = target->sprite->y - missile->sprite->y;
  adx = abs(dx);
  ady = abs(dy);

  if (adx>640 || ady>480) {
	printf(" WARNING: dx,dy values out of range (%d,%d)\n",
		   dx,dy);
	missile->sprite->spdx = 0;
	missile->sprite->spdy = 0;
	return;
  }

  missile->sprite->spdx = isign(dx)*HMISSILE_SPD_X(adx,ady);
  missile->sprite->spdy = isign(dy)*HMISSILE_SPD_Y(adx,ady);

  missile->anim_frame = hmissile_frame(missile->sprite->spdx, 
									   missile->sprite->spdy);

  //  PROFILE_END;

}
