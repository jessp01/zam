
#ifndef __SPRITE_H_
#define __SPRITE_H_

#include <SDL/SDL.h>

typedef struct {
  int	x,y;					/* Sprite position */
  int	w,h;					/* Sprite size */
  int	spdx, spdy;				/* Sprite speed */

  SDL_Surface *image;			/* Sprite SDL surface */
  SDL_Surface *surf;			/* Screen */

} sprite_t;


sprite_t *sprite_init(const char *name, SDL_Surface *);
sprite_t *sprite_init_empty(int w, int h, SDL_Surface *);
void sprite_setpos(sprite_t *, int, int);
int sprite_draw(sprite_t *);
int sprite_collide(sprite_t *, sprite_t *);

#endif /* __SPRITE_H_ */

