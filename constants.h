
#ifndef __CONSTANTS_H_
#define __CONSTANTS_H_

#define MAX_BULLETS 10			/* Maximum number of bullets on screen */

#define MAX_ENEMIES 30			/* Maximum number of enemies on screen */

#define MAX_FLOATS 10			/* Maximum number of floating strings  */

#define MAX_BONI 10				/* Maximum number of bonus objects  */

#define MAX_CONTEMP_EVENTS 10	/* Maximum number of events which
								 * can happen at the same time    */

#define MAX_HERO_ENERGY 8		/* Maximum (and initial) hero's energy */
#define MAX_HERO_HMISSILE 5		/* Maximum number of homing missiles */

#endif /*  __CONSTANTS_H_ */
