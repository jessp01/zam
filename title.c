#include <stdlib.h>
#include <SDL/SDL.h>

#include "zam.h"
#include "font.h"

#define NSTARS 250

static SDL_Surface *screen;
static opts_t *optsp;

/***************************
 *                         *
 *        Starfield        *
 *                         *
 ***************************/

typedef struct {
  int x,y;						/* star coords */
  int level;					/* star level */
  int speed;					/* star speed */
  int delay;					/* delay counter */
} star_t;

star_t stars[NSTARS];

void init_starfield() {
  int i, rnd;

  SDL_FillRect(screen, NULL, 0);

  srand(0);
  for (i=0; i<NSTARS; i++) {
	stars[i].y = -1;
	rnd = 10.0*rand()/RAND_MAX;
	stars[i].level = 1+((rnd<6)?0:((rnd<9)?1:2));
	stars[i].x = (float)SCREEN_WIDTH * rand()/RAND_MAX;
	stars[i].y = (float)SCREEN_HEIGHT * rand()/RAND_MAX;
	stars[i].speed = (4-stars[i].level)*(1+5.0*rand()/RAND_MAX);
	stars[i].delay = stars[i].speed;
  }

}

void update_starfield() {

  int i, col;

  SDL_LockSurface(screen);
  for (i=0; i<NSTARS; i++) {
	if (stars[i].y != -1) {
	  //	  putpixel(screen, stars[i].x, stars[i].y, 0);

	  if (stars[i].delay)
		stars[i].delay--;
	  else {
		stars[i].delay = stars[i].speed;
		stars[i].y++;
	  }

	  if (stars[i].y >= SCREEN_HEIGHT)
		stars[i].y = -1;
	  else {
		switch (stars[i].level) {
		  case 1:
			col = SDL_MapRGB(screen->format, 0x80, 0x80, 0x80);
			break;
		  case 2:
			col = SDL_MapRGB(screen->format, 0xa0, 0xa0, 0xa0);
			break;
		  case 3:
			col = SDL_MapRGB(screen->format, 0xff, 0xff, 0xff);
			break;
		}
		putpixel(screen, stars[i].x, stars[i].y, col);
	  }
	} else {
	  stars[i].speed = (4-stars[i].level)*(1+3.0*rand()/RAND_MAX);
	  stars[i].delay = stars[i].speed;
	  stars[i].x = (float)SCREEN_WIDTH * rand()/RAND_MAX;
	  stars[i].y = 0;
	}
  }
  SDL_UnlockSurface(screen);

}



/***************************
 *                         *
 *       Title screen      *
 *                         *
 ***************************/

typedef enum {
  MENU_QUIT = -1,
  MENU_START = 0,
  MENU_OPTS,
  MENU_MAIN
} menu_t;

menu_t options_menu() {
  SDL_Event event;
  const char *opts_audio[] = { "OFF", "FX only", "Muzak", "FX+Muzak" };

  /* XXX Warning: options selection is not working
   * (some changes are required in the main menu in order
   * to init background, fullscreen and audio each time
   * the relevant option is changed). Playing with this menu
   * at the moment will result in a segfault.
   */
  draw_string(FONT_16, 140, 100, "Options [NOT WORKING YET!]");
  draw_string(FONT_16, 140, 240, "B      background [%s]",
			  optsp->background?"ON":"OFF");
  draw_string(FONT_16, 140, 260, "F      fullscreen [%s]",
			  optsp->fullscreen?"ON":"OFF");
  draw_string(FONT_16, 140, 280, "A      audio [%s]", 
			  opts_audio[optsp->audio]);
  draw_string(FONT_16, 140, 320, "ESC    back to main");

  while (SDL_PollEvent(&event)) {
	switch (event.type) {
	  case SDL_KEYDOWN:
		switch (event.key.keysym.sym) {
		  case SDLK_b:
			optsp->background = ~optsp->background;
			break;
		  case SDLK_f:
			optsp->fullscreen = ~optsp->fullscreen;
			break;
		  case SDLK_a:
			optsp->audio++;
			break;
		  case SDLK_ESCAPE:
		  case SDLK_q:
			return MENU_MAIN;
		  default:
			break;
		}
		break;
		
	  case SDL_QUIT:
		return MENU_QUIT;
		
	  default:
		break;
	}
  }

  return MENU_OPTS;
}

int main_menu() {
  SDL_Event event;

  draw_string(FONT_16, 200, 100, "Welcome to ZAM");
  draw_string(FONT_8,  235, 120, "(C) 2022 By Jessex");
  draw_string(FONT_16, 180, 280, "F1/SPACE    start");
  draw_string(FONT_16, 180, 300, "O           options");
  draw_string(FONT_16, 180, 320, "ESC         quit");

  while (SDL_PollEvent(&event)) {
	switch (event.type) {
	  case SDL_KEYDOWN:
		switch (event.key.keysym.sym) {
		  case SDLK_ESCAPE:
		  case SDLK_q:
			return MENU_QUIT;
		  case SDLK_F1:
		  case SDLK_SPACE:
			return MENU_START;
		  case SDLK_o:
			return MENU_OPTS;
		  default:
			break;
		}
		break;
		
	  case SDL_QUIT:
		return MENU_QUIT;
		
	  default:
		break;
	}
  }
  
  return 2;
}

int title_screen(SDL_Surface *scr, opts_t *opts) {

  int next;
  int (*loopf)() = main_menu;

  screen = scr;
  optsp = opts;

  init_starfield();

  while (1) {
	SDL_FillRect(screen, NULL, 0);
	next = loopf();
	switch (next) {
	  case MENU_MAIN:
		loopf = main_menu;
		break;
	  case MENU_OPTS:
		loopf = options_menu;
		break;
	  case MENU_START:
	  case MENU_QUIT:
		return next;
	}
	update_starfield();
	SDL_Flip(screen);
  }  

}
