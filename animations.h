
#ifndef __ANIMATIONS_H_
#define __ANIMATIONS_H_

#include <SDL/SDL.h>

#define MAX_ANIM_LEN 30

/* A bitmap sequence. Used for animations */
typedef struct {
  SDL_Surface *surface[MAX_ANIM_LEN];
  int n;						/* how many frames */
  int speed;					/* animation speed */
} anim_seq_t;


typedef enum {
  ANIM_EXPLOSION_32,
  ANIM_EXPLOSION_64,
  ANIM_ENEMY_DRONE,
  ANIM_ENEMY_RUSHER,
  ANIM_ASTEROIDG_32,
  ANIM_ASTEROIDG_64,
  ANIM_ASTEROIDG_EXPL,
  ANIM_HERO,
  ANIM_HERO_MISSILE,
  ANIM_HERO_HMISSILE,
  ANIM_BONUS_DIVE,
  ANIM_BONUS_INVULN,
  ANIM_BONUS_MISSILE,
  ANIM_BONUS_SCORE,
  ANIM_BONUS_SHIELD,
  ANIM_BONUS_SPEED,
  ANIM_BONUS_WEAPON,
  ANIM_MAX
} anim_type_t;


void init_animations();
anim_seq_t *get_animation(anim_type_t);

#endif /* __ANIMATIONS_H_ */

