/***************************************************************************
 * ZAM                                                                     *
 * (C) 2022 Jessex                                                         *
 *                                                                         *
 * Original porting from Xenon2000: Phavon                                 *
 * (C) 2002 Gianluca Insolvibile (branzo@users.sourceforge.net)            *
 * http://phavon.sourceforge.net/                                          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 ***************************************************************************/

#ifndef PHAVON_H
#define PHAVON_H

#include <assert.h>

#include "objects.h"

#define GNAME "ZAM"
#define ZAM_VERSION "0.0.1"


/* Key defines */
#define KEY_UP		SDLK_UP
#define KEY_DOWN	SDLK_DOWN
#define KEY_LEFT	SDLK_LEFT
#define KEY_RIGHT	SDLK_RIGHT
#define KEY_FIRE	SDLK_SPACE
//#define KEY_FIRE	SDLK_LCTRL
#define KEY_FIRE2	SDLK_LALT


/* Options struct */
typedef struct {
  unsigned int fullscreen:1;				/* fullscreen ? */
  unsigned int autoshoot:1;				/* autoshoot ? */
  unsigned int background:1;				/* background ? */
  unsigned int incr:1;					/* incremental map row update */
  unsigned int audio:2;					/* audio mode */
  unsigned int layers;					/* number of layers */
  unsigned int background_speed;			/* background scroll speed */
} opts_t;

/* Player struct */
typedef struct {
  object_t *hero;
  int		lives;
  unsigned int	score;
  int		bullet_type;
  int		hmissile;
  char*		name;
} player_t;
#define HERO(p) (p->hero)
#define HERO_SPR(p) (HERO(p)->sprite)

#define SCREEN_WIDTH  640
#define SCREEN_HEIGHT 480
#endif
