
#include <stdio.h>
#include <sys/time.h>

#include "constants.h"

typedef struct event {
  struct event	*next;
  int			virt_time;
  struct {
	int			object_family;	/* Object family */
	int			object_type;	/* Object type */
	int			x,y;			/* Object x,y position */
	int			param1, param2;	/* Opaque params */
  } events[MAX_CONTEMP_EVENTS];
} event_t;

static event_t *script = NULL;

void event_enqueue(event_t *);
void dump_script();

#define NEW_EVENT(evt,time)						\
{												\
  evt = (event_t *)malloc(sizeof(event_t));		\
  for (i=0; i<MAX_CONTEMP_EVENTS; i++)			\
	evt->events[i].object_type = -1;			\
  evn = 0;										\
  evt->virt_time = time;						\
}

void init_script(int duration) {
  int i;
  event_t *evt = NULL;

#ifdef LEVEL_AUTOGEN
  for (i=0; i<duration/100; i++) {
	NEW_EVENT(evt,i*100);
	event_enqueue(evt);
  }
#else
  FILE *fp;
  char buffer[80];
  int f,t,x,y,p1,p2,line=0,res;
  int last_time = -1, evn = 0;

  fp = fopen("level1.scr", "r");
  if (!fp) return;
  while (!feof(fp)) {
	if (!fgets(buffer, 80, fp)) break;
	line++;
	if (buffer[0]=='#') continue;
	res = sscanf(buffer, "%d %d %d %d %d %d", &f, &t, &x, &y, &p1, &p2);
	if (res == 1) {
	  //	  printf("[%d] Event time %d\n", line, f);
	  if (f > duration) {
		printf("[%d] Warning: event at non-existent time (%d)\n",
			   line, f);
	  }
	  if (f != last_time) {
		if (f < last_time) {
		  printf("[%d] Warning: non sequential events in script\n",
				 line);
		}
		NEW_EVENT(evt,f);
		event_enqueue(evt);
	  }
	} else if (res == 6) {
	  //	  printf("[%d] Event data: %d %d %d %d %d %d\n", line, 
	  //			 f, t, x, y, p1, p2);
	  if (!evt)
		printf("[%d] Script syntax error\n", line);
	  else {
		evt->events[evn].x = x;
		evt->events[evn].y = y;
		evt->events[evn].object_family = f;
		evt->events[evn].object_type = t;
		evt->events[evn].param1 = p1;
		evt->events[evn].param2 = p2;
		if (++evn >= MAX_CONTEMP_EVENTS) {
		  int newtime = evt->virt_time+1;
		  NEW_EVENT(evt,newtime);
		  event_enqueue(evt);
		}
	  }
	} else
	  printf("[%d] wrong number of arguments\n", line);
  }

  fclose(fp);
#endif /* LEVEL_AUTOGEN */

  /* We always need an end-of-time fake event */
  evt = (event_t *)malloc(sizeof(event_t));
  evt->virt_time = 1000000;		/* infinite */
  for (i=0; i<MAX_CONTEMP_EVENTS; i++)
	evt->events[i].object_type = -1;
  event_enqueue(evt);

  //  dump_script();

}

void destroy_script() {
  event_t *next, *cursor = script;

  while (cursor) {
	next = cursor->next;
	free(cursor);
	cursor = next;
  }

}

void run_script(int vtime) {

  static event_t *next_event = NULL;
  static int last_evt_vtime = 0;
  int i;

  if (last_evt_vtime > vtime) {
	/* Virtual time has gone backwards, restart script */
	next_event = script;
  }

  if (!next_event)
	next_event = script;

  if (next_event->virt_time > vtime)
	return;

  /*
  if (next_event->virt_time < vtime)
	fprintf(stderr, "Warning: late event "
			"(vtime %d, event scheduled for %d)\n",
			vtime, next_event->virt_time);
  */

  last_evt_vtime = vtime;
  for (i=0; i<MAX_CONTEMP_EVENTS; i++) {

	if (next_event->events[i].object_type == -1)
	  break;
	new_object(next_event->events[i].object_family, 
			   next_event->events[i].object_type, 
			   next_event->events[i].x, 
			   next_event->events[i].y,
			   next_event->events[i].param1,
			   next_event->events[i].param2);
  }
  next_event = next_event->next;

}

void event_enqueue(event_t *evt) {

  // TBD: enqueuing should be ordered
  // (i.e. later events come next)

  event_t *cursor = script;

  if (!evt)
	return;

  evt->next = 0;

  if (!script) {
	script = evt;
	return;
  }

  while (cursor && cursor->next)
	cursor = cursor->next;
  if (!cursor) {
	fprintf(stderr, "internal script list error\n");
	return;
  }
  cursor->next = evt;

}

void dump_script() {
  int i;
  event_t *cursor = script;

  printf("Script Dump:\n");
  while (cursor) {
	for (i=0; i<MAX_CONTEMP_EVENTS; i++) {
	  if (cursor->events[i].object_type == -1)
		break;
	  printf("VTime %d: family %d, type %d @ %d,%d (p1 %d, p2 %d)\n", 
			 cursor->virt_time, cursor->events[i].object_family, 
			 cursor->events[i].object_type, 
			 cursor->events[i].x, cursor->events[i].y, 
			 cursor->events[i].param1,
			 cursor->events[i].param2);
	}
	cursor = cursor->next;
  }

}
