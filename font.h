#ifndef __FONT_H_
#define __FONT_H_

#include <stdarg.h>

typedef enum {
  FONT_8,
  FONT_16,
  FONT_MAX
} font_type_t;


/* Font functions protos */
void init_font(SDL_Surface *scr);
void release_font();
void draw_char(font_type_t font, int x, int y, char c);
void draw_string(font_type_t font, int x, int y, const char *format, ...);
void draw_char_surf(SDL_Surface *ds, font_type_t font, int x, int y, char c);
void draw_string_surf(SDL_Surface *ds, font_type_t font, int x, int y, 
					  const char *format, ...);
void vdraw_string_surf(SDL_Surface *ds, font_type_t font, int x, int y, 
					   const char *format, va_list ap);


#endif /* __FONT_H_ */
