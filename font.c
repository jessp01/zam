
#include <stdio.h>
#include <SDL/SDL.h>

#include "zam.h"
#include "font.h"
#include "datafiles.h"

static SDL_Surface *screen;

static struct {
  const char	*filename;
  int			w,h;
  SDL_Surface	*bitmap;
} font_props[FONT_MAX] = {
  { FILE_FONT8, 8, 8, NULL },
  { FILE_FONT16, 16, 16, NULL }
};

void init_font(SDL_Surface *scr) {

  int i;
  SDL_Surface *tmp;

  screen = scr;

  for (i=0; i<FONT_MAX; i++) {
	tmp = SDL_LoadBMP(font_props[i].filename);
	if (!tmp) {
	  fprintf(stderr, 
			  "cannot load %s\n", font_props[i].filename);
	}
	SDL_SetColorKey(tmp, SDL_SRCCOLORKEY, 
					SDL_MapRGB(tmp->format, 0xff, 0x00, 0xff));
	font_props[i].bitmap = SDL_DisplayFormat(tmp);
	SDL_FreeSurface(tmp);
  }

}

void release_font() {
  int i;
  for (i=0; i<FONT_MAX; i++)
	if (font_props[i].bitmap)
	  SDL_FreeSurface(font_props[i].bitmap);
}


/*******************************************
 *  Functions to draw in the public screen *
 *******************************************/

void draw_char(font_type_t font, int x, int y, char c) {

  SDL_Rect srcrect, dstrect;

  srcrect.w = font_props[font].w;
  srcrect.h = font_props[font].h;
  dstrect.x = x;
  dstrect.y = y;

  c -= 0x20;
  srcrect.x = (c%8)*font_props[font].w;
  srcrect.y = (c/8)*font_props[font].h;

  SDL_BlitSurface(font_props[font].bitmap, &srcrect, screen, &dstrect);

}

void draw_string(font_type_t font, int x, int y, const char *format, ...) {

  int i, len, x1;
  char string[1024];

  va_list ap;
  va_start(ap, format);
  vsprintf(string, format, ap);
  va_end(ap);

  len = strlen(string);
  if (x==-1) {
	x1 = SCREEN_WIDTH - len*font_props[font].w;
  } else
	x1 = x;

  if (y==-1)
	y = SCREEN_HEIGHT - font_props[font].h;

  for (i=0; i<len; i++) {
	draw_char(font, x1, y, string[i]);
	x1 += font_props[font].w;
  }

}



/*******************************************
 *  Functions to draw in a generic surface *
 *******************************************/

void draw_char_surf(SDL_Surface *ds, font_type_t font, int x, int y, char c) {

  SDL_Rect srcrect, dstrect;

  srcrect.w = font_props[font].w;
  srcrect.h = font_props[font].h;
  dstrect.x = x;
  dstrect.y = y;

  c -= 0x20;
  srcrect.x = (c%8)*font_props[font].w;
  srcrect.y = (c/8)*font_props[font].h;

  SDL_BlitSurface(font_props[font].bitmap, &srcrect, ds, &dstrect);

}

void draw_string_surf(SDL_Surface *ds, font_type_t font, int x, int y, 
					  const char *format, ...) {

  int i, len, x1;
  char string[1024];

  va_list ap;
  va_start(ap, format);
  vsprintf(string, format, ap);
  va_end(ap);

  len = strlen(string);
  if (x==-1) {
	x1 = ds->w - len*font_props[font].w;
  } else
	x1 = x;

  if (y==-1)
	y = ds->h - font_props[font].h;

  for (i=0; i<len; i++) {
	draw_char_surf(ds, font, x1, y, string[i]);
	x1 += font_props[font].w;
  }

}

void vdraw_string_surf(SDL_Surface *ds, font_type_t font, int x, int y, 
					  const char *format, va_list ap) {

  int i, len, x1;
  char string[1024];

  vsprintf(string, format, ap);

  len = strlen(string);
  if (x==-1) {
	x1 = ds->w - len*font_props[font].w;
  } else
	x1 = x;

  if (y==-1)
	y = ds->h - font_props[font].h;

  for (i=0; i<len; i++) {
	draw_char_surf(ds, font, x1, y, string[i]);
	x1 += font_props[font].w;
  }

}
